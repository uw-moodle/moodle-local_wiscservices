<?php
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Forms used by "Add a UW Person" interface
 *
 * @package    local
 * @subpackage wiscservices
 * @copyright  2014 University of Wisconsin
 * @author     Matt petro
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

require_once($CFG->libdir.'/formslib.php');

class wisc_adduwperson_form extends moodleform {

    function definition() {
        $mform = $this->_form;

        $mform->addElement('header', 'header', get_string('adduwpeople', 'local_wiscservices'));
        $datasources = array (  'netid'   => 'NetID or NetID@wisc.edu',
                                'emplid'  => 'EMPLID',
                                'photoid' => 'PHOTOID',
                                'pvi'     => 'PVI',
                                'name'    => 'Name (preview only)');
        $mform->addElement('select', 'datasource', get_string('datasource', 'local_wiscservices'), $datasources);
        $mform->setDefault('datasource', 'UW-Madison NetID');
        $mform->addRule('datasource', null, 'required');
        $mform->addElement('textarea','uwids', get_string('uwids', 'local_wiscservices'), array('rows'=>10, 'cols'=>40));
        $mform->addHelpButton('uwids', 'uwids', 'local_wiscservices');
        $mform->addRule('uwids', null, 'required');

        $buttonarray=array();
        $buttonarray[] = &$mform->createElement('submit', 'preview', get_string('preview'));
        $buttonarray[] = &$mform->createElement('submit', 'submitbutton', get_string('addpeople', 'local_wiscservices'));
        $buttonarray[] = &$mform->createElement('cancel');
        $mform->addGroup($buttonarray, 'buttonar', '', array(' '), false);
        $mform->closeHeaderBefore('buttonar');
    }
}
