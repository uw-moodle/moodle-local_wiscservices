<?php
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Search for and add a UW account to the system
 *
 * @package    local
 * @subpackage wiscservices
 * @copyright  2014 University of Wisconsin
 * @author     Matt petro
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

use \local_wiscservices\local\uds\uds_datasource;
use \local_wiscservices\local\uds\uds_query;
use \local_wiscservices\local\wisc\wisc_person;
use local_wiscservices\local\uds\local_wiscservices\local\uds;

require_once('../../config.php');
require_once($CFG->libdir.'/adminlib.php');
require_once($CFG->dirroot.'/local/wiscservices/locallib.php');
require_once($CFG->dirroot.'/local/wiscservices/adduwperson_form.php');

require_login($SITE->id);
require_capability('local/wiscservices:adduser', context_system::instance());

// Handle the bulk user actions button
// This is a bit of a hack, but there's no clean way to prepopulate the bulk user selection
$bulkuser = optional_param('bulkuser', 0, PARAM_INT);
if ($bulkuser) {
    // Redirect to the bulk user actions page, with the user list prepopulated
    $ids = required_param('ids', PARAM_RAW);
    $ids = explode(',', $ids);
    $SESSION->bulk_users = array();
    foreach ($ids as $id) {
        if (is_number($id)) {
            $SESSION->bulk_users[] = $id;
        }
    }
    redirect(new moodle_url('/admin/user/user_bulk.php'));
}

$PAGE->set_pagelayout('admin');
$PAGE->set_heading(get_string('adduwpeople', 'local_wiscservices'));
$PAGE->set_title(get_string('adduwpeople', 'local_wiscservices'));

$thisurl = new moodle_url('/local/wiscservices/adduwperson.php');
$prevurl = new moodle_url('/');

$PAGE->set_url($thisurl);

$wiscservices = new local_wiscservices_plugin();

$mform = new wisc_adduwperson_form();

if ($mform->is_cancelled()) {
    redirect($prevurl);
}

echo $OUTPUT->header();
if ($data = $mform->get_data()) {
    require_sesskey();

    $datasource = $data->datasource;
    $ispreview = !empty($data->preview);

    if ($datasource == "name" && !$ispreview) {
        echo $OUTPUT->notification(get_string('cantaddbyname', 'local_wiscservices'), 'notifyproblem');
        $ispreview = true; // Force preview when querying by name (inexact)
    }

    // process data if submitted
    if ($datasource == "name") {
        // Split by line for names
        $uwids = preg_split('/\n/', $data->uwids);
    } else {
        // Split by word for other identifiers
        $uwids = preg_split("/[\s,]+/", $data->uwids);
    }

    $getudsperson = new uds_datasource();

    // Some UDS name queries can take a long time, e.g. searching for 'Kim'.  It's unfortunate
    // that we have to make this so large.
    $getudsperson->set_timeout(30);

    // Convert EPPNs to NetIDs
    if ($datasource === "netid") {
        foreach ($uwids as &$netid) {
            $matches = array();
            if (preg_match('/^([^@]+)@wisc.edu$/', $netid, $matches)) {
                $netid = $matches[1];
            }
        }
    }

    // Preview table
    $table = new html_table;
    $table->head = array(
            get_string("uwid", "local_wiscservices"),
            get_string("name"),
            get_string("email"),
            get_string("uwroles", "local_wiscservices"),
            get_string("netid", "local_wiscservices"),
            get_string("pvi", "local_wiscservices"),
            get_string("action"),
    );
    $table->id = 'adduwpersonpreview';
    $table->data = array();

    $toupdate = array();

    $querymethod = "get_people_by_".$datasource; // $datasource is validated by form

    // Query uds for each record.
    // We do these one at a time so that we can catch missing data.
    foreach ($uwids as $uwid) {
        if (empty($uwid)) {
            continue;
        }

        $people = uds_query::{$querymethod}($getudsperson, $uwid);

        //  process results
        if ($people) {
            // save for update later.  The only time we'll have more that one result is
            // when querying by name, but in that case we only allow preview.  So, here just remember the first person.
            $toupdate[$uwid] = reset($people);

            foreach ($people as $person) {
                // build preview table row
                $params = array('idnumber'=>$person->pvi,
                                    'auth'=>get_config('local_wiscservices', 'authtype'),
                                 'deleted'=>0);
                if ($DB->record_exists('user', $params)) {
                    $status = get_string('userupdate', 'local_wiscservices');
                } else {
                    $status = get_string('usercreate', 'local_wiscservices');
                }
                $uwroles = implode(',',$person->roles);
                if (empty($person->email) || $wiscservices->is_valid_email($person->email)) {
                    $email = $person->email;
                } else {
                    $email = get_string('bogusemail', 'local_wiscservices', $person->email);
                }
                $row = array(s($uwid), s($person->firstName).' '.s($person->lastName), s($email), s($uwroles),
                             s($person->netid), s($person->pvi), $status);
                $table->data[] = $row;
            }
        } else {
            echo $OUTPUT->notification(get_string('usernotfound', 'local_wiscservices', s($uwid)), 'notifyproblem');
        }
    }

    if (!$ispreview) {
        // update each user
        $updatedids = array();
        foreach ($toupdate as $uwid=>$person) {
            try {
                if ($userid = $wiscservices->verify_person($person)) {
                    $updatedids[] = $userid;
                    echo $OUTPUT->notification(get_string('processeduser', 'local_wiscservices', s($uwid)), 'notifysuccess');
                } else {
                    echo $OUTPUT->notification(get_string('erroruser', 'local_wiscservices', s($uwid)), 'notifyproblem');
                }
            } catch (Exception $e) {
                echo $OUTPUT->notification(get_string('erroruser', 'local_wiscservices', s($uwid)).' ('.$e->getMessage().')', 'notifyproblem');
            }
        }
    }

    if ($ispreview) {
        // preview, so print the form again
        echo $OUTPUT->box_start();
        echo $OUTPUT->heading(get_string('preview'));
        echo html_writer::table($table);
        echo $OUTPUT->box_end();
        $mform->display();
    } else {
        // done, so print a continue button
        $continue = new single_button($thisurl, get_string('continue'), 'get');
        echo html_writer::tag('div', $OUTPUT->render($continue), array('class' => 'buttons'));
        $bulkurl = new moodle_url($thisurl, array('ids'=>implode(',', $updatedids), 'bulkuser'=>1));
        $bulkuser = new single_button($bulkurl, get_string('bulkuser', 'local_wiscservices'), 'post');
        echo html_writer::tag('div', $OUTPUT->render($bulkuser), array('class' => 'buttons'));
    }

} else {
    // display form
    $mform->display();
}

echo $OUTPUT->footer();
