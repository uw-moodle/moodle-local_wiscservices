<?php

/**
 * CLI sync for full Peoplepicker synchronisation.
 */

/**
 *
 * This script is meant to be called from a cronjob to sync uw account information.
 *
 * Example cron entry:
 * # 5 minutes past 4am
 * 5 4 * * * /usr/bin/php5 -c /etc/php5/cli/php.ini /var/www/moodle/local/wiscservices/cli/syncusers.php
 *
 * Notes:
 *   - For debugging & better logging, you are encouraged to use in the command line:
 *     -d log_errors=1 -d error_reporting=E_ALL -d display_errors=0 -d html_errors=0
 *
 */

define('CLI_SCRIPT', true);

require_once(dirname(dirname(dirname(dirname(__FILE__)))).'/config.php');
require_once($CFG->dirroot.'/local/wiscservices/locallib.php');

// Ensure errors are well explained
$CFG->debug = DEBUG_NORMAL;

// Update users
$wiscservices = new local_wiscservices_plugin();
$progress = new text_progress_trace();
$wiscservices->sync_users(null, true, $progress);
$progress->finished();


