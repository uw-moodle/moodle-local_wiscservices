<?php

defined('MOODLE_INTERNAL') || die();

$plugin->version   = 2016031000;
$plugin->requires  = 2012120300;   // See http://docs.moodle.org/dev/Moodle_Versions
$plugin->release   = '3.3.1';
$plugin->cron      = 0;
$plugin->component = 'local_wiscservices';
$plugin->maturity  = MATURITY_RC;

$plugin->dependencies = array(
);
