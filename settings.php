<?php

/**
 * WISC local plugin global settings
 *
 */

defined('MOODLE_INTERNAL') || die();

require_once($CFG->dirroot.'/course/lib.php');
require_once($CFG->dirroot.'/local/wiscservices/locallib.php');
require_once($CFG->libdir.'/authlib.php');

$modname = 'local_wiscservices';
$plugin = 'local/wiscservices';

if ($hassiteconfig) {
    $settings = new admin_settingpage($modname, get_string('pluginname', 'local_wiscservices'));
    $ADMIN->add('localplugins', $settings);

    //--- heading ---
    $settings->add(new admin_setting_heading('local_wiscservices', '', get_string('pluginname_desc', 'local_wiscservices')));

    if (!class_exists('SoapClient')) {
        $settings->add(new admin_setting_heading('enrol_phpsoap_noextension', '', get_string('phpsoap_noextension', 'local_wiscservices')));
    } else {

        $yesno = array(get_string('no'), get_string('yes'));

        //--- peoplepicker settings ---
        $settings->add(new admin_setting_heading('local_wiscservices_peoplepicker_settings', get_string('peoplepickerserver_settings', 'local_wiscservices'), ''));
        $settings->add(new admin_setting_configtext('local_wiscservices/peoplepickerurl', get_string('peoplepickerurl_key', 'local_wiscservices'), '', 'http://esb.services.wisc.edu/esbv2/UWDS/WebService/IMS.wsdl', PARAM_URL,60));
        $settings->add(new admin_setting_configtext('local_wiscservices/peoplepickeruser', get_string('peoplepickeruser_key', 'local_wiscservices'), '', '', PARAM_RAW));
        $settings->add(new admin_setting_configpasswordunmask('local_wiscservices/peoplepickerpass', get_string('peoplepickerpass_key', 'local_wiscservices'), '', '', PARAM_RAW));

        //--- getudsperson settings ---
        $settings->add(new admin_setting_heading('local_wiscservices_udsperson_settings', get_string('udspersonserver_settings', 'local_wiscservices'), ''));
        $settings->add(new admin_setting_configtext('local_wiscservices/udspersonurl', get_string('udspersonurl_key', 'local_wiscservices'), '', 'http://esb.services.wisc.edu/esbv2/UWDS/WebService/UDSPerson2.wsdl', PARAM_URL,60));
        $settings->add(new admin_setting_configtext('local_wiscservices/udspersonuser', get_string('udspersonuser_key', 'local_wiscservices'), '', '', PARAM_RAW));
        $settings->add(new admin_setting_configpasswordunmask('local_wiscservices/udspersonpass', get_string('udspersonpass_key', 'local_wiscservices'), '', '', PARAM_RAW));
        $settings->add(new admin_setting_configcheckbox('local_wiscservices/fakemixedcase', get_string('fakemixedcase_key', 'local_wiscservices'), get_string('fakemixedcase', 'local_wiscservices'), 1));

        //--- UDS webservice settings (getPVIChangeHistory) ---
        $settings->add(new admin_setting_heading('local_wiscservices_udsservice_settings', get_string('udsservice_settings', 'local_wiscservices'), ''));
        $settings->add(new admin_setting_configtext('local_wiscservices/udsserviceurl', get_string('udsserviceurl_key', 'local_wiscservices'), '', 'http://esb.services.wisc.edu/esbv2/UWDS/WebService/UDS.wsdl', PARAM_URL,60));
        $settings->add(new admin_setting_configtext('local_wiscservices/udsserviceuser', get_string('udsserviceuser_key', 'local_wiscservices'), '', '', PARAM_RAW));
        $settings->add(new admin_setting_configpasswordunmask('local_wiscservices/udsservicepass', get_string('udsservicepass_key', 'local_wiscservices'), '', '', PARAM_RAW));

        $settings->add(new admin_setting_heading('local_wiscservices_caeapi_settings', get_string('caeapi_settings', 'local_wiscservices'), ''));
        $settings->add(new admin_setting_configtext('local_wiscservices/caeusername', get_string('caeusername', 'local_wiscservices'), '', '', PARAM_RAW));
        $settings->add(new admin_setting_configpasswordunmask('local_wiscservices/caepassword', get_string('caepassword', 'local_wiscservices'), '', '', PARAM_RAW));

        //--- uw account settings ---
        $settings->add(new admin_setting_heading('local_wiscservices_account_settings', get_string('account_settings', 'local_wiscservices'), ''));
        $auths = core_component::get_plugin_list('auth');
        $options = array();
        foreach($auths as $authname=>$path) {
            $authplugin = get_auth_plugin($authname);
            /// Get the auth title (from core or own auth lang files)
            $options[$authname] = $authplugin->get_title();
        }
        $settings->add(new admin_setting_configselect('local_wiscservices/authtype',
                get_string('authtype_key', 'local_wiscservices'), get_string('authtype', 'local_wiscservices'), 'shibboleth', $options));
        $options = $yesno;
        $settings->add(new admin_setting_configselect('local_wiscservices/allowguestnetids', get_string('allowguestnetids_key', 'local_wiscservices'), get_string('allowguestnetids', 'local_wiscservices'), 0, $options));
        $options = array(AUTH_REMOVEUSER_KEEP        => get_string('auth_remove_keep', 'auth'),
                         AUTH_REMOVEUSER_SUSPEND     => get_string('auth_remove_suspend', 'auth'),
                         AUTH_REMOVEUSER_FULLDELETE  => get_string('auth_remove_delete', 'auth'));
        $settings->add(new admin_setting_configselect('local_wiscservices/removeuser', get_string('auth_remove_user_key', 'auth'), get_string('auth_remove_user', 'auth'), AUTH_REMOVEUSER_KEEP, $options));
        $options = $yesno;
        $settings->add(new admin_setting_configselect('local_wiscservices/development', get_string('development_key', 'local_wiscservices'), get_string('development', 'local_wiscservices'), 0, $options));
        $settings->add(new admin_setting_configfile('local_wiscservices/bogusemailfile', get_string('bogusemailfile_key', 'local_wiscservices'), get_string('bogusemailfile', 'local_wiscservices'), ''));
    }
}

//$PAGE->settingsnav->get('usercurrentsettings')->add("UW settings", new moodle_url('/', array('setdefaulthome'=>true)), navigation_node::TYPE_SETTING);
//$usersetting = $this->add(get_string($gstitle, 'moodle', $fullname), null, self::TYPE_CONTAINER, null, $key);

//--- course creator ---
// we need this hook since the enrol/wisc settings file isn't loaded for non-admin users
if (file_exists($CFG->dirroot.'/enrol/wisc/accesslib.php')) {
    require_once($CFG->dirroot.'/enrol/wisc/accesslib.php');
    wisc_settings_local_hook($ADMIN);
}

//--- UW people picker ---
$ADMIN->add('users', new admin_externalpage('adduwpeople', get_string('adduwpeople','local_wiscservices'), "$CFG->wwwroot/local/wiscservices/adduwperson.php", array('local/wiscservices:adduser')));