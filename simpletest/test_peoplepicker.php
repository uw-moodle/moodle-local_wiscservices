<?php
/**
 * Unit tests for Peoplepicker
 *
 * These tests require that appropriate test data is provided in the moodle config.php file.
 *
 * Sample data:
 *
 * $CFG->local_wiscservices_test = (object) array (
 *  'netid'   => "...",
 *  'pvi'     => "...",
 *  'emplid'  => "...",
 *  'hrid'    => "...",
 * );
 *
 */

if (!defined('MOODLE_INTERNAL')) {
    die('Direct access to this script is forbidden.'); //  It must be included from a Moodle page
}

// Make sure the code being tested is accessible.
require_once($CFG->dirroot . '/local/wiscservices/lib/peoplepicker.php'); // Include the code to test

class test_peoplepicker extends UnitTestCase {

	public $data;  // test data from $CFG->enrol_wisc_test

	public function __construct() {
		global $CFG;

		$this->data = $CFG->local_wiscservices_test;
	}

	public function setUp() {
	    if (empty($this->data)) {
            throw new Exception('NO TEST DATA.  Peoplepicker tests require $CFG->local_wiscservices_test to be set in config.php');
        }
	    $this->pp = new wisc_peoplepicker();
	}

	public function tearDown() {
		unset ($this->ds);
	}

    public function test_getPeopleByNetid() {
        $people = $this->pp->getPeopleByNetid($this->data->netid);
        $this->assertTrue(count($people) == 1, "getPeopleByNetid");
        $this->assertTrue(is_a(reset($people), 'wisc_person'), "getPeopleByNetid");
    }

    public function test_getPeopleByPvi() {
        $people = $this->pp->getPeopleByPvi($this->data->pvi);
        $this->assertTrue(count($people) == 1, "getPeopleByPvi");
        $this->assertTrue(is_a(reset($people), 'wisc_person'), "getPeopleByPvi");
    }

    public function test_getPeopleByEmplid() {
        $people = $this->pp->getPeopleByEmplid($this->data->emplid);
        $this->assertTrue(count($people) == 1, "getPeopleByEmplid");
        $this->assertTrue(is_a(reset($people), 'wisc_person'), "getPeopleByEmplid");
    }

    public function test_getPeopleByHrid() {
        $people = $this->pp->getPeopleByHrid($this->data->hrid);
        $this->assertTrue(count($people) == 1, "getPeopleByHrid");
        $this->assertTrue(is_a(reset($people), 'wisc_person'), "getPeopleByHrid");
    }

	public function test_getPeopleByNetidDNE() {
		$people = $this->pp->getPeopleByNetid('nosuchnetid');
		$this->assertTrue(count($people) == 0, "test_getPeopleByNetidDNE");
    }
}
?>