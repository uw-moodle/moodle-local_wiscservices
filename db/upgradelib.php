<?php

/**
 * Local wiscservices upgrade related helper functions
 */

defined('MOODLE_INTERNAL') || die();

/**
 * Migrate old moodle 1.9 netid accounts
 *
 * This code makes assumptions about how the new accounts will operate: namely
 *      auth='shibboleth' and
 * 	    eppn_usernames='yes'
 * Ideally we should read config data, but that hasn't been setup by the time this runs.
 *
 * @return void
 */
function wiscservices_20_migrate() {
    global $CFG, $DB, $OUTPUT;

    if (get_config('local_wiscservices', '20_migrate_done')) {
        return;
    }

    $dbman = $DB->get_manager();
    if ($dbman->table_exists('auth_netid_usermap')) {
        echo $OUTPUT->notification('Updating netid accounts', 'notifysuccess');
        $maps = $DB->get_recordset('auth_netid_usermap', array(), '', 'idnumber, pvi, netid');
        foreach ($maps as $map) {
            $user = $DB->get_record('user', array('idnumber'=>$map->idnumber), 'id,auth,username,deleted');
            if ($user) {
                if ($user->auth === 'netid' || $user->username === $map->netid || $user->deleted || $user->auth === 'nologin') {
                    $usernew = new stdClass();
                    $usernew->id = $user->id;
                    $update = false;
                    if (!empty($map->pvi)) {
                        $usernew->idnumber = $map->pvi;
                        $update = true;
                    }
                    if ($user->auth === 'netid') {
                        $usernew->auth = 'shibboleth';
                        $update = true;
                    }
                    if (strpos($user->username, '@') === false) {
                        $usernew->username = $user->username . '@wisc.edu';
                        $update = true;
                    }
                    if ($update) {
                        $DB->update_record('user', $usernew);
                    }
                }
            }
        }
        $maps->close();
    }
    // remove old merge table
    $tablename = 'auth_netid_extuser';
    $table = new xmldb_table($tablename);
    if ($dbman->table_exists($table)) {
        $dbman->drop_table($table);
    }

    // TODO: delete old usermap table.  We're keeping it around for
    // now in case we need it to fix problems.
    //if ($oldversion < 2011000000) {
            //$tablename = 'auth_netid_usermap';
            //$table = new xmldb_table($tablename);
            //if ($dbman->table_exists($table)) {
            //    $dbman->drop_table($table);
            //}
            //echo $OUTPUT->notification('Droping old table', 'notifysuccess');
        //upgrade_plugin_savepoint(true, 2011000000, 'local', 'wiscservices');
    //}

    // Record that we finished the migration
    set_config('local_wiscservices', 1, '20_migrate_done');
}
