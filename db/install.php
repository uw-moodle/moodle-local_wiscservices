<?php

/**
 * Post installation and migration code.
 *
 * This file replaces:
 *   - STATEMENTS section in db/install.xml
 *   - lib.php/modulename_install() post installation hook
 *   - partially defaults.php
 *
 */

defined('MOODLE_INTERNAL') || die();

function xmldb_local_wiscservices_install() {
    global $CFG;

    // Upgrade from old resource module type if needed
    require_once("$CFG->dirroot/local/wiscservices/db/upgradelib.php");
    wiscservices_20_migrate();
}

function xmldb_local_wiscservices_install_recovery() {
    global $CFG;

    require_once("$CFG->dirroot/local/wiscservices/db/upgradelib.php");
    wiscservices_20_migrate();
}