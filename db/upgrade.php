<?php

/**
 * Keeps track of upgrades to the local_wiscservices plugin
 */

/**
 * @param int $oldversion the version we are upgrading from
 * @return bool result
 */
function xmldb_local_wiscservices_upgrade($oldversion) {
    global $DB, $CFG, $OUTPUT;

    $dbman = $DB->get_manager();

    $result = true;

    if ($oldversion < 2014070202) {

        // eppn usernames are now required

        // Force setting and append @wisc.edu to all usernames which don't have it

        // Append @wisc.edu to all usernames in our authtype
        if (get_config('local_wiscservices', 'eppnusername')) {
            $sql = "UPDATE {user} SET username = CONCAT(username, '@wisc.edu')
                     WHERE username NOT LIKE '%@%'
                       AND deleted = 0
                       AND auth = :auth";
            $params = array('auth' => get_config('local_wiscservices', 'authtype'));
            $DB->execute($sql, $params);
        }

        set_config('eppnusername', 1, 'local_wiscservices');

        // archive savepoint reached
        upgrade_plugin_savepoint(true, 2014070202, 'local', 'wiscservices');
    }

    if ($oldversion < 2015041600) {

        // Try to update config to the new ESB url
        foreach (array('peoplepickerurl', 'udspersonurl') as $setting) {
            $soapurl = get_config('local_wiscservices', $setting);
            $soapurl = preg_replace('/\.services\.wisc\.edu\/UWDS/', '.services.wisc.edu/esbv2/UWDS', $soapurl);
            set_config($setting, $soapurl, 'local_wiscservices');
        }
        // savepoint reached
        upgrade_plugin_savepoint(true, 2015041600, 'local', 'wiscservices');
    }
    if ($oldversion < 2015061100) {

        // Update the uwOverride custom field.
        $field = $DB->get_record('user_info_field', array('shortname' => 'uwOverride'));
        if ($field) {
            $field->param1 = local_wiscservices_plugin::OVERRIDE_NONE."\n"
                            .local_wiscservices_plugin::OVERRIDE_NAME."\n"
                            .local_wiscservices_plugin::OVERRIDE_EMAIL."\n"
                            .local_wiscservices_plugin::OVERRIDE_ALL;
            $DB->update_record('user_info_field', $field);
        }
        // savepoint reached
        upgrade_plugin_savepoint(true, 2015061100, 'local', 'wiscservices');
    }

    return true;
}
