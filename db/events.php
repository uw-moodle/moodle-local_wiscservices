<?php

/**
 * WISC local plugin event handlers
 */

defined('MOODLE_INTERNAL') || die();

$handlers = array (
    'user_created' => array (
        'handlerfile'      => '/local/wiscservices/eventlib.php',
        'handlerfunction'  => array('local_wiscservices_handler', 'user_created'),
        'schedule'         => 'instant'
    ),
);