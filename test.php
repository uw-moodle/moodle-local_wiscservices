<?php

    require_once '../../config.php';

    require_login(0, false);
    $context = context_system::instance();
    require_capability('moodle/site:config', $context);

    require_once( "lib/peoplepicker.php" );

    $timestart  = time();

    //mtrace("Server Time: ".date('r',$timestart)."<br/><br/>");

    try {
        mtrace("Opening peoplepicker<br/>");

        $peoplepicker = new wisc_peoplepicker();

        mtrace("Calling getPeopleByNetid...");
        if (!empty($CFG->local_wiscservices_test->netid)) {
            // test with real netid
            $netid = $CFG->local_wiscservices_test->netid;
            $people = $peoplepicker->getPeopleByNetid($netid);
            if (count($people) != 1) {
                throw new Exception("Got no result for netid '$netid'.  This netid is set in the moodle config.php file.");
            }
        } else {
            // test with fake netid and see if we get an exception
            $people = $peoplepicker->getPeopleByNetid('nosuchnetid');
        }
    } catch( Exception $e ) {
       mtrace("<br/><br/><strong>Test Failed: ".$e->getMessage()."</strong>");
       exit();
    }

    mtrace("<br/><br/><strong>Test Succeeded</strong>");
    mtrace("<br/><br/>elapsed time: ". (time() - $timestart) . "s");
?>
