<?php
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * University of Wisconsin datasource plugin
 *
 * @package    local
 * @subpackage wiscservices
 * @copyright  2014 University of Wisconsin
 * @author     Matt petro
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

use \local_wiscservices\local\uds\datasource;
use \local_wiscservices\local\uds\uds_datasource;
use \local_wiscservices\local\uds\uds_query;
use \local_wiscservices\local\wisc\wisc_person;

defined('MOODLE_INTERNAL') || die();

require_once($CFG->libdir.'/authlib.php');
require_once($CFG->dirroot.'/user/lib.php');
require_once($CFG->dirroot.'/user/profile/lib.php');

/**
 * local_wiscservices_plugin class
 *
 * Utility class for communicating with the campus getUDSPerson datasource
 *
 * @package    local
 * @subpackage wiscservices
 * @copyright  2014 University of Wisconsin
 * @author     Matt petro
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class local_wiscservices_plugin {

    /** @var datasourse */
    protected $datasource;

    // cache for custom profile field ids
    protected $custom_fields;

    // error logging prefix
    protected $errorlogtag = '[LOCAL WISC] ';

    /** @var array Cache of pvi->userid for users we've processed. */
    protected $idmap = array();

    /** @var progress_trace $trace */
    protected $trace;

    // Custom profile field override settings (i.e. moodle user fields which shouldn't be updated)
    const OVERRIDE_NONE  = "None";  // No override
    const OVERRIDE_NAME  = "Name";  // Override just the name.
    const OVERRIDE_EMAIL = "Email";  // Override just the email.
    const OVERRIDE_ALL   = "All";  // Override name and email.

    public function __construct(progress_trace $trace = null) {
        if (!$trace) {
            $trace = new error_log_progress_trace();
        }
        $this->trace = $trace;
    }

    /**
     * Set the UDS datasource.
     *
     * If this the datasource is not set, then the standard UDS datasource will be used.
     *
     * @param datasource $datasource
     */
    public function set_datasource(datasource $datasource) {
        $this->datasource = $datasource;
    }

    /**
     * Get our datasource service class, creating it if necessary
     *
     * @return uds_datasource | null
     */
    protected function get_datasource() {
        if (!isset($this->datasource)) {
            try {
               $this->datasource = new uds_datasource();
            } catch (Exception $e) {
               $this->trace->output($this->errorlogtag.$e->getMessage());
               return null;
            }
        }
        return $this->datasource;
    }

    /**
     * Sync moodle user data from our datasource
     *
     * This function throws exceptions on SOAP errors.
     *
     * @param array|null $userids null for all users
     * @param boolean $delete Should we process user suspension/deletion on missing external data?
     * @param progress_trace $progress
     * @return null;
     */
    public function sync_users(array $userids = null, $delete = true, progress_trace $progress = null) {
        global $DB;

        if (is_null($progress)) {
            $progress = new null_progress_trace();
        }

        $this->init_customfields();

        $progress->output("==Syncing users==");

        // Initialize the datasource.
        if (!$datasource = $this->get_datasource()) {
            $progress->output("Error: Unable to open datasource");
            return;
        }
        $datasource->set_timeout(90);
        $pagesize = $datasource->get_maxquery();

        // Get userprofile field for emplid
        $emplidfield = $this->get_customfield_id('emplid');

        // Construct our user query SQL
        if (!empty($userids)) {
            list($useridsql, $useridparams) = $DB->get_in_or_equal($userids, SQL_PARAMS_NAMED, 'uid');
            $useridsql = " AND u.id $useridsql";
        } else {
            list($useridsql, $useridparams) = array("", array());
        }

        $fullsync = empty($userids);
        if ($fullsync) {
            // If we are doing a full sync, then don't worry about suspended users.  They are unlikely to reappear
            $suspendedsql = " AND u.suspended = 0";
        } else {
            // If we are looking for specific users, then consider suspended accounts as well
            $suspendedsql = "";
        }

        // Get all moodle accounts with the right auth type
        $sql = "SELECT u.id, u.suspended, u.username, u.idnumber, u.firstname, u.lastname, u.email, d.data as emplid
                  FROM {user} u
             LEFT JOIN {user_info_data} d ON u.id = d.userid AND d.fieldid = :emplidfield
                 WHERE u.auth = :auth AND u.deleted = 0 $useridsql $suspendedsql";
        $params = array('auth'=>get_config('local_wiscservices', 'authtype'),
                'emplidfield'=>$emplidfield);
        $params = $params + $useridparams;

        $userlist = $DB->get_recordset_sql($sql, $params);

        // Users not found in datasource.
        $todelete = array();

        // Number of users updated
        $numupdates = 0;
        $savednumupdates = 0; // For logging purposes

        // Process users in chunks of size $pagesize.
        while ($userlist->valid()) {

            // Batch $pagesize users.
            $batch = array();
            for ($cnt = 0 ; $cnt < $pagesize && $userlist->valid(); ++$cnt) {
                $batch[$userlist->key()] = $userlist->current();
                $userlist->next();
            }

            // First try matching by pvi (user record idnumber).

            // Create an index array.
            $bypvi = array();
            foreach ($batch as $user) {
                if (!empty($user->idnumber)) {
                    $bypvi[$user->idnumber] = $user;
                }
            }

            // Lookup by pvi.
            $people = uds_query::get_people_by_pvi($datasource, array_keys($bypvi));

            // Match by pvi, and remove from $batch
            foreach ($people as $person) {
                if (!empty($person->pvi) && isset($bypvi[$person->pvi])) {
                    $user = $bypvi[$person->pvi];
                    $this->update_user($user, $person);
                    // Unset so that we don't process the user again
                    unset($batch[$user->id]);
                    $numupdates++;
                    unset($user);
                }
            }
            unset($bypvi, $people);

            if (!empty($batch)) {
                // Some didn't match by pvi, so query individually.  This catches pvi changes.
                foreach ($batch as $user) {
                    if ($user->idnumber) {
                        $person = uds_query::get_people_by_pvi($datasource, array($user->idnumber));
                        $person = reset($person);
                        if ($person) {
                            if ($user->idnumber != $person->pvi) {
                                $this->trace->output($this->errorlogtag."PVI change for $user->id ($user->idnumber -> $person->pvi).");
                            }
                            $this->update_user($user, $person);
                            unset($batch[$user->id]);
                        }
                        unset($person);
                    }
                }
            }

            if (!empty($batch)) {
                // Some didn't match by pvi, so lookup by netid.

                // Create an index array.
                $bynetid = array();
                foreach ($batch as $user) {
                    $netid = $this->username_to_netid($user->username);
                    if ($netid) {
                        $bynetid[$netid] = $user;
                    }
                    unset($netid);
                }

                // Lookup by netid.
                $people = uds_query::get_people_by_netid($datasource, array_keys($bynetid));

                // Match by netid and remove from $batch
                foreach ($people as $person) {
                    if (!empty($person->netid) && isset($bynetid[$person->netid])) {
                        $user = $bynetid[$person->netid];
                        $this->update_user($user, $person);
                        // Unset so that we don't process the user again
                        unset($batch[$user->id]);
                        $numupdates++;
                        unset($user);
                    }
                }
                unset ($bynetid, $people);
            }

            if (!empty($batch)) {
                // Some records didn't match pvi or netid, so lookup by emplid.

                // Create an index array.
                $byemplid = array();
                foreach ($batch as $user) {
                    if (!empty($user->emplid)) {
                        $byemplid[$user->emplid] = $user;
                    }
                }

                // Lookup by emplid.
                $people = uds_query::get_people_by_emplid($datasource, array_keys($byemplid));

                // Match by emplid and remove from $batch
                foreach ($people as $person) {
                    if (!empty($person->emplid) && isset($byemplid[$person->emplid])) {
                        $user = $byemplid[$person->emplid];
                        $this->update_user($user, $person);
                        // Unset so that we don't process the user again
                        unset($batch[$user->id]);
                        $numupdates++;
                        unset($user);
                    }
                }
                unset($byemplid, $people);
            }

            if (!empty($batch)) {
                // No match on pvi, netid, emplid, so add to a list to delete/suspend later.
                $todelete = $todelete + $batch;
            }
            unset($batch);
            if ($numupdates - $savednumupdates >= 200) {
                $progress->output("Updated $numupdates users");
                $savednumupdates = $numupdates;
            }
        }
        $progress->output("Updated $numupdates users");

        // Done processing userlist query
        $userlist->close();

        // Delete or suspend users
        if ($delete) {
            if (!empty($todelete) && $numupdates == 0 && empty($userids)) {
                // No users found in datasource when doing a full sync, so something must be wrong
                $progress->output("Did not get any users from datasource -- error?\n");
                return;
            }
            // Delete or suspend users as configured
            $removeuser = get_config('local_wiscservices', 'removeuser');
            foreach ($todelete as $user) {
                if ($removeuser == AUTH_REMOVEUSER_FULLDELETE) {
                    if (delete_user($user)) {
                        $progress->output(get_string('auth_dbdeleteuser', 'auth_db', array('name'=>$user->username, 'id'=>$user->id)));
                    } else {
                        $progress->output(get_string('auth_dbdeleteusererror', 'auth_db', array('name'=>$user->username, 'id'=>$user->id)));
                    }
                } else if ($removeuser == AUTH_REMOVEUSER_SUSPEND) {
                    $updateuser = new stdClass();
                    $updateuser->id = $user->id;
                    $updateuser->suspended = 1;
                    user_update_user($updateuser, false);
                    \core\session\manager::kill_user_sessions($user->id);
                    $progress->output(get_string('auth_dbsuspenduser', 'auth_db', array('name'=>$user->username, 'id'=>$user->id)));
                    unset($updateuser);
                } else {
                    // Keep user
                    $progress->output("User $user->username id $user->id missing from external datasource.");
                }
                $numupdates++;
            }
        }
        $progress->output("Synchronized $numupdates users");
    }

    /**
     * Update a single moodle user's data by calling getUDSPerson.  This is called from the user creation event
     * handler.
     *
     * This function catches SOAP exceptions and returns false on error.
     *
     * @param mixed $user the moodle user or userid
     * @return bool true on success
     */
    public function sync_user($user) {
        if (is_object($user)) {
            $userid = $user->id;
        } else {
            $userid = $user;
        }
        try {
            $this->sync_users(array($userid), false);
        } catch (Exception $e) {
           $this->trace->output($this->errorlogtag.$e->getMessage());
           return false;
        }
        return true;
    }

    /**
     * Generate the username from the netid or pvi
     *
     * @param string $netid
     * @param string $pvi  used if netid is missing
     * @return string|null
     */
    public function netid_to_username($netid, $pvi) {
        if (strpos($netid, '@') !== false) {
            return null;
        }
        if (!empty($netid)) {
            $username = strtolower($netid) . '@wisc.edu';
        } else {
            // No netid, so set the username to something unique (pvi and netid cannot both be empty)
            if (empty($pvi)) {
                return null;
            }
            $username = 'missingnetid.'.$pvi.'@wisc.edu';
            // Moodle requires lowercase usernames
            $username = strtolower($username);
        }
        return $username;
    }

    /**
     * Generate the netid from the username, if possible
     *
     * @param string $username
     * @return string|null
     */
    public function username_to_netid($username) {
        if (preg_match('/^missingnetid/', $username)) {
            // we don't have a netid for the person
            return null;
        }
        $matches = array();
        if (preg_match('/^(.*)@wisc\.edu$/', $username, $matches)) {
            // wisc domain
            $netid = $matches[1];
        } else {
            // not wisc, so ignore this username
            $netid = null;
        }
        return $netid;
    }

    /**
     * Check if this is an @wisc.edu account
     *
     * @param string $username
     * @return boolean
     */
    public function is_wisc_username($username) {
        if (preg_match('/.@wisc\.edu$/', $username)) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Update a person's data in moodle and return the moodle userid. This
     * function will create a new moodle account if the person doesn't exist.
     *
     * @param wisc_person $person
     * @param bool $update should we update existing data (slower)
     * @return int|false   moodle userid or false on error
     */
    public function verify_person(wisc_person $person, $update=true) {
        // Class request cache, mainly for the wisc/enrol plugin which
        // can call this function repeatedly for the same user.

        if (!empty($person->pvi) && isset($this->idmap[$person->pvi])) {
            return $this->idmap[$person->pvi];
        }

        $result = $this->_verify_person($person, $update);
        if (!empty($person->pvi)) {
            $this->idmap[$person->pvi] = $result;
        }
        return $result;
    }

    /**
     * Protected implementation of verify_person()
     */
    protected function _verify_person(wisc_person $person, $update) {
        global $DB;
        global $CFG;

        require_once($CFG->dirroot.'/user/profile/lib.php');

        $authtype = get_config('local_wiscservices', 'authtype');
        if ($authtype === false) {
            throw new Exception($this->errorlogtag.'no authtype defined');
        }

        if ($update) {
            $userfields = 'id,username,idnumber,suspended,firstname,lastname,email';
        } else {
            $userfields = 'id';
        }

        $user = false;
        $username = $this->netid_to_username($person->netid, $person->pvi);
        if (!empty($person->pvi)) {
            // Find user by pvi
            $params = array('idnumber'=>$person->pvi,
                            'auth'=>$authtype,
                            'deleted'=>0);
            $user = $DB->get_record('user', $params, $userfields);
        }
        if ($user === false && !empty($username)) {
            // That didn't work, so try by username
            $params = array('username'=>$username,
                            'auth'=>$authtype,
                            'deleted'=>0);
            $user = $DB->get_record('user', $params, $userfields);
        }
        if ($user === false && !empty($person->emplid)) {
            // That didn't work, so try by emplid
            $fieldid = $this->get_customfield_id('emplid');
            $sql = "SELECT u.* FROM {user} u
                               JOIN {user_info_data} d ON u.id = d.userid AND d.fieldid=:emplidfield
                               WHERE u.deleted = 0 AND u.auth=:auth AND d.data=:emplid";
            $params = array('emplidfield'=>$fieldid, 'auth'=>$authtype, 'emplid'=>$person->emplid);
            $user = $DB->get_record_sql($sql, $params, IGNORE_MISSING);
        }
        if ($user === false && !empty($person->pvi)) {
            // That didn't work, so check the PVI change history
            $pvihistory = $this->get_datasource()->get_pvi_change_history($person->pvi);
            if (!empty($pvihistory)) {
                list ($pvisql,$params) = $DB->get_in_or_equal($pvihistory, SQL_PARAMS_NAMED);
                $params['auth'] = $authtype;
                $users = $DB->get_records_select('user', "idnumber $pvisql AND auth=:auth AND deleted=0", $params);
                if (count($users) > 0) {
                    if (count($users) > 1) {
                        $this->trace->output($this->errorlogtag."Duplicate users in PVI change history for $person->pvi");
                    }
                    $user = reset($users);
                    $update = 1;  // Force an update if an old PVI was found.
                }
                unset($users);
            }
        }
        if ($user === false) {
            // Still not found, so create a new account

            if (!$this->is_valid_person_for_create($person, true)) {
                return false;
            }

            $user = new stdClass;
            $user->username = $username;
            $user->idnumber = $person->pvi;

            $fakemixedcase = (boolean) get_config('local_wiscservices', 'fakemixedcase');

            $user->firstname = self::compute_updated_name(null, $person->firstName, $fakemixedcase);
            $user->lastname = self::compute_updated_name(null, $person->lastName, $fakemixedcase);;

            if (!empty($person->email) && $this->is_valid_email($person->email)) {
                $user->email = $person->email;
            } else {
                $user->email = "";  // Empty string, not null, because of DB schema.
            }
            if (!$person->ferpaEmail) {
                $user->maildisplay = 2; // only classmates can see
            } else {
                $user->maildisplay = 0; // none can see
            }

            $user->auth = $authtype;
            $user->confirmed  = 1;
            $user->modified   = time();
            $user->mnethostid = $CFG->mnet_localhost_id;
            $user->lang = $CFG->lang;

            try {
                $user->id = user_create_user($user, false, false);

                $this->update_user_profile($user, $person);

                // Call the user_created event handler
                \core\event\user_created::create_from_userid($user->id)->trigger();
                $this->trace->output($this->errorlogtag.get_string('createduser', 'local_wiscservices', $user->id));

            } catch (moodle_exception $e) {
                $this->trace->output($this->errorlogtag."Unable to create user $user->idnumber: ".$e->getMessage());
                return false;
            }

        } else if ($update) {
            // Update user
            $this->update_user($user, $person);
        }
        return $user->id;
    }

    /**
     * Find a moodle user by uw identifier
     *
     * @param string $identifier  One of: 'photoid', 'netid', 'eppn', 'emplid', 'pvi'
     * @param string $value
     * @return object|false   moodle user or false if not found.
     */
    public function find_user($identifier, $value) {
        global $DB,$CFG;
        if (empty($value)) {
            return null;
        }
        $authtype = get_config('local_wiscservices', 'authtype');
        switch ($identifier) {
            case 'netid':
                $username = $this->netid_to_username($value, null);
                if (empty($username)) {
                    return null;
                }
                $user = $DB->get_record('user', array('username'=>$username, 'deleted'=>0, 'auth'=>$authtype, 'mnethostid'=>$CFG->mnet_localhost_id));
                break;
            case 'eppn':
                $user = $DB->get_record('user', array('username'=>$value, 'deleted'=>0, 'auth'=>$authtype, 'mnethostid'=>$CFG->mnet_localhost_id));
                break;
            case 'pvi':
                $user = $DB->get_record('user', array('idnumber'=>$value, 'deleted'=>0, 'auth'=>$authtype, 'mnethostid'=>$CFG->mnet_localhost_id));
                break;
            case 'eppn':
            case 'photoid':
                $fieldid = $this->get_customfield_id($identifier);
                $sql = "SELECT u.*
                          FROM {user} u
                          JOIN {user_info_data} d ON u.id = d.userid AND d.fieldid = :fieldid
                         WHERE u.auth = :auth AND u.deleted = 0 AND d.data = :search";
                $params = array('auth'=>get_config('local_wiscservices', 'authtype'),
                        'fieldid'=>$fieldid, 'search'=>$value);
                $user = $DB->get_record_sql($sql, $params);
                break;
            default:
                throw new coding_exception('Unknown identifier for search.');
        }
        return $user;
    }


    /**
     * Find a moodle user by pvi.
     *
     * This function will create a new moodle account if the person doesn't exist.
     *
     * @param string $pvi
     * @param bool $update should we update existing data by always querying UDS? (slower)
     * @param bool $requirenetid should we abort if there is no netid?
     * @return object|false   moodle user or false on error
     */
    public function get_user_by_pvi($pvi, $update=false, $requirenetid=false) {
        global $DB;
        if (empty($pvi)) {
            throw new coding_exception('Empty PVI passed to get_user_by_pvi');
        }
        $select = 'idnumber = ?
                       AND username LIKE \'%@wisc.edu\'
                       AND deleted = 0
                       AND auth = ?';
        $params = array($pvi, get_config('local_wiscservices', 'authtype'));
        $user = $DB->get_record_select('user', $select, $params, '*', IGNORE_MISSING);
        if ($user) {
            // Found a moodle user.

            // Force an update if we don't have a netid.
            if (!$this->username_to_netid($user->username)) {
                $update = 1;
            }

            if ($update) {
                $this->sync_user($user);
            }
            return $user;
        } else {
            // Didn't find a moodle user, so query UDS.
            $datasource = $this->get_datasource();
            $person = uds_query::get_people_by_pvi($datasource, $pvi);
            $person = reset($person);
            if (!$person) {
                $this->trace->output($this->errorlogtag." No UDS record for '$pvi'");
                return false;
            }
            if (!$person->netid && $requirenetid) {
                $this->trace->output($this->errorlogtag." No Netid for '$pvi'");
                return false;
            }
            $userid = $this->verify_person($person, $update);
            if (!$userid) {
                return false;
            }
            $user = $DB->get_record('user', array('id'=>$userid));
            return $user;
        }

        // unreachable.
        return false;
    }

    /**
     * Get the datasource person corresponding to a moodle user
     *
     * @param object $user user object
     * @return wisc_person|null
     */
    public function get_person($user) {
        if ($user->auth !== get_config('local_wiscservices', 'authtype')) {
            return null;
        }

        $datasource = $this->get_datasource();
        $person = array();
        if (!empty($user->idnumber)) {
            $person = $datasource->getPeopleByPvi($user->idnumber);
        } else {
            $netid = static::username_to_netid($user->username);
            if (is_null($netid)) {
                return null;
            }
            $person = $datasource->getPeopleByNetid($netid);
        }
        $person = reset($person);
        return $person;
    }

    /**
     * Determine if a wisc_person is valid for creating a new account.
     *
     * @param wisc_person $person
     * @return bool
     */
    public function is_valid_person_for_create(wisc_person $person, $logerror = true) {
        $valid = true;

        $missing = array();
        $errors = '';

        // Block Guest netids if configured
        $isguestnetid = strpos($person->netid, 'tmp-') === 0;
        if ($isguestnetid && !get_config('local_wiscservices', 'allowguestnetids')) {
            $valid = false;
            $errors .= 'Blocking guest netid. ';
        }

        // Require pvi
        if (empty($person->pvi)) {
            $valid = false;
            $missing[] = 'PVI';
        }

        // Netid is optional due to L3 requirements.
        if (empty($person->netid)) {
            $missing[] = 'netid';
        }

        // Email is optional.
        if (empty($person->email)) {
            $missing[] = 'email';
        }


        // Require a last name, unless it's a Guest netid
        if (!$isguestnetid && empty($person->lastName)) {
            $valid = false;
            $missing[] = 'lastname';
        }

        if (!empty($missing)) {
            $errors .= "Missing " . implode(',', $missing);
        }

        if (!empty($errors)) {
            if ($valid) {
                $this->trace->output("UDS validation WARNING for '$person->pvi' : $errors");
            } else {
                $this->trace->output("UDS validation ERRORS for '$person->pvi' : $errors");
            }
        }
        return $valid;

    }

    /**
     * Compute the updated name for a user given the existing name and the new external name.
     *
     * This is a bit complicated because we can get both mixed case and all-caps names from both UDS and CHUB.
     *
     * We always prefer mixed case names, so we need to preserve those when available.  When only
     * all-caps names are available, we use those and (optionally) heuistically convert to mixed-case in the process.
     *
     * @param string $oldname
     * @param string $newname
     * @param bool $fakemixedcase
     */
    protected static function compute_updated_name($oldname, $newname, $fakemixedcase) {
        if (empty($newname)) {
            return $oldname;
        }
        $newnameallcaps = strlen($newname) > 1 && (strtoupper($newname) == $newname);

        // Handle conversion to lower case.
        if ($newnameallcaps && $fakemixedcase) {
            // Try to convert to mixed case if needed.
            $newname = preg_replace_callback('/([^a-z]|^)([a-z])/',
                    function($matches) {return $matches[1].strtoupper($matches[2]);},strtolower($newname));
        }

        if (empty($oldname)) {
            return $newname;
        }
        $oldnameallcaps = strlen($oldname) > 1 && (strtoupper($oldname) == $oldname);

        if ($oldnameallcaps || is_null($oldname)) {
            // The old name is null or all caps, so we should update regardless.  This should only happen once per account.
            $result = $newname;
        } else if (!$newnameallcaps) {
            // New name is mixed case, so use it.  It is authoritative.
            $result = $newname;
        } else if (0 == strcasecmp($newname, $oldname)) {
            // Names are the same up to case, so keep the previous mixed case name since it might have
            // come from an authoritative mixed-case source.
            $result = $oldname;
        } else {
            // Names are different up to case, so use the new name.
            $result = $newname;
        }
        return $result;
    }

    /**
     * Update a moodle user in the db from a wisc_person object
     *
     * @param object $user user object
     * @param wisc_person $person
     */
    protected function update_user($user, wisc_person $person) {
        global $DB, $CFG;

        // Load custom profile fields if necessary
        if (!isset($user->profile)) {
            profile_load_custom_fields($user);
        }

        $fakemixedcase = (boolean) get_config('local_wiscservices', 'fakemixedcase');

        // Check for overrides
        $overridename = false;
        $overrideemail = false;
        if (!empty($user->profile)) {
            if (isset($user->profile['uwOverride'])) {
                if ($user->profile['uwOverride'] == self::OVERRIDE_NAME || $user->profile['uwOverride'] == self::OVERRIDE_ALL) {
                    $overridename = true;
                }
                if ($user->profile['uwOverride'] == self::OVERRIDE_EMAIL || $user->profile['uwOverride'] == self::OVERRIDE_ALL) {
                    $overrideemail = true;
                }
            }
        }

        // Check the user record
        $toupdate = array();
        if (!$overridename) {
            // Process preferred first name
            if ($person->supports_preferred_names()) {
                $firstname = self::compute_updated_name($user->firstname, $person->firstName, $fakemixedcase);
            } else {
                // Don't update firstname if our datasource does not provide preferred names
                // Note: we are expecting to have full preferred name support by Fall 2014
                $firstname = null;
            }
            if ($firstname && $firstname !== $user->firstname) {
                $toupdate['firstname'] = $firstname;
            }
            // Process last name
            $lastname = self::compute_updated_name($user->lastname, $person->lastName, $fakemixedcase);
            if ($lastname && $lastname !== $user->lastname) {
                $toupdate['lastname'] = $lastname;
            }
        }

        $has_demographic_data = !empty($person->lastName);
        if (!$overrideemail) {
            if (!empty($person->email) && $this->is_valid_email($person->email)) {
                // Upstream has an email address, so update our end if needed.
                if ($user->email != $person->email) {
                    $toupdate['email'] = $person->email;
                }
            } else if ($has_demographic_data){
                // Upstream has no email address.  If this is the first time we noticed this, then we clear
                // our local email address.  If we've already cleared it (uwNoEmail is set) then we leave it alone
                // since the user may have entered something manually.
                if (empty($user->profile['uwNoEmail']) && !empty($user->email)) {
                    $toupdate['email'] = '';
                    // Note that update_user_profile will now set $user->profile['uwNoEmail']).
                }
            } else {
                // UWMOODLE-773
                // No demographic data, so who knows what the correct state should be.  Don't clear the local email address.
                // getUDSPerson tends to give us records like this.
            }
        }

        if (!empty($person->pvi) && $user->idnumber != $person->pvi) {
            $toupdate['idnumber'] = $person->pvi;
        }
        $username = $this->netid_to_username($person->netid, $person->pvi);
        if (!empty($username) && $user->username != $username) {
            // See if there is a duplicate username
            if ($existinguser = $DB->get_record('user', array('username'=>$username, 'mnethostid'=>$CFG->mnet_localhost_id), 'id,idnumber')) {
                $this->trace->output($this->errorlogtag."Duplicate username '$username'  OLD: $existinguser->id ($existinguser->idnumber), NEW: $user->id ($user->idnumber)");
            } else {
                $toupdate['username'] = $username;
            }
        }
        // Handle unsuspension if configured to do so
        if (!empty($user->suspended)) {
            if (get_config('local_wiscservices', 'removeuser') == AUTH_REMOVEUSER_SUSPEND) {
                $toupdate['suspended'] = 0;
            } else {
                // leave user suspended if we're not configured to suspend automatically
            }
        }
        // Handle Ferpa email flag.  Only process this if it was just set upstream,
        // since the person may want to override the setting
        if ($person->ferpaEmail && empty($user->profile['ferpaEmail'])) {
            $toupdate['maildisplay'] = 0; // none can see
        }

        // Now update the DB if necessary
        if ($toupdate) {
            $toupdate['id'] = $user->id;
            try {
                user_update_user($toupdate, false);
            } catch (moodle_exception $e) {
                $this->trace->output($this->errorlogtag."Unable to update user $user->id: ".$e->getMessage());
            }
        }

        // Update custom fields
        $this->update_user_profile($user, $person);
    }

    /**
     * Update custom profile fields for a moodle user from a timetable record
     *
     * @param object $user user object with existing custom profile fields loaded
     * @param wisc_person $person
     */
    protected function update_user_profile($user, wisc_person $person) {

        // Check the custom profile fields

        if (empty($user->profile)) {
            $user->profile = array();
        }

        // netid
        if(isset($person->netid)){
            if (!isset($user->profile['netid']) || $user->profile['netid'] != $person->netid) {
                $this->set_custom_field('netid', $person->netid, $user->id);
            }
        }
        // emplid
        if (isset($person->emplid)) {
            if (!isset($user->profile['emplid']) || $user->profile['emplid'] != $person->emplid) {
                $this->set_custom_field('emplid', $person->emplid, $user->id);
            }
        }
        // photoid
        if (isset($person->photoid)) {
            if (!isset($user->profile['photoid']) || $user->profile['photoid'] != $person->photoid) {
                $this->set_custom_field('photoid', $person->photoid, $user->id);
            }
        }
        //ferpaName
        if (isset($person->ferpaName)) {
            if (!isset($user->profile['ferpaName']) || $user->profile['ferpaName'] != $person->ferpaName) {
                $this->set_custom_field('ferpaName', $person->ferpaName, $user->id);
            }
        }
        //ferpaEmail
        if (isset($person->ferpaEmail)) {
            if (!isset($user->profile['ferpaEmail']) || $user->profile['ferpaEmail'] != $person->ferpaEmail) {
                $this->set_custom_field('ferpaEmail', $person->ferpaEmail, $user->id);
            }
        }
        //uwRoles
        if (isset($person->roles)) {
            sort($person->roles);
            $roles = implode(',', $person->roles);
            if (!isset($user->profile['uwRoles']) || $user->profile['uwRoles'] != $roles) {
                $this->set_custom_field('uwRoles', $roles, $user->id);
            }
        }
        // uwNoEmail
        // This is a flag to indicate that upstream has no email address and we allow a local email address.  We need
        // this so that we don't overwrite a manually set email with the upstream blank email, except the very first
        // time the upstream email disappears.  For example, if a wiscmail account goes away, we want to clear the local
        // address, but allow the user to set something else.
        if (empty($person->email) || !$this->is_valid_email($person->email)) {
            // Set uwNoEmail
            if (empty($user->profile['uwNoEmail'])) {
                $this->set_custom_field('uwNoEmail', 1, $user->id);
            }
        } else {
            // Clear uwNoEmail
            if (!empty($user->profile['uwNoEmail'])) {
                $this->set_custom_field('uwNoEmail', 0, $user->id);
            }
        }
    }

    /**
     * Set a profile custom field
     *
     * @param int $field custom field shortname
     * @param string $data field data
     * @param int $userid user id
     */
    protected function set_custom_field($field, $data, $userid) {
        global $DB;

        $fieldid = $this->get_customfield_id($field);
        if ($fieldid === false) {
            $this->trace->output($this->errorlogtag."Custom profile field $field doesn't exist");
            return;
        }
        $customfield = $DB->get_record('user_info_data', array('userid'=>$userid, 'fieldid'=>$fieldid));
        if ($customfield) {
            $customfield->data = $data;
            $DB->update_record('user_info_data', $customfield);
        } else {
            $customfield = new stdClass();
            $customfield->userid = $userid;
            $customfield->fieldid = $fieldid;
            $customfield->data = $data;
            $DB->insert_record('user_info_data', $customfield);
        }
    }

    protected function get_customfield_id($shortname) {
        global $DB;

        $fields = $this->init_customfields();
        return $fields[$shortname];
    }

    /**
     * Initialize our custom user profile fields, creating them if necessary.
     *
     * @return array of the form shortname => fieldid
     */
    protected function init_customfields() {
        global $DB;

        if ($this->custom_fields) {
            return $this->custom_fields; // only need to do this once
        }

        // Make sure the custom user profiles fields are installed,
        $fieldshortnames = array('netid', 'ferpaName', 'ferpaEmail', 'emplid', 'photoid', 'uwRoles', 'uwOverride', 'uwNoEmail');
        list($where, $params) = $DB->get_in_or_equal($fieldshortnames, SQL_PARAMS_QM);
        $fields = $DB->get_records_select_menu('user_info_field', "shortname $where", $params, '', 'id,shortname');

        if (array_diff($fieldshortnames, $fields)) {
            // Some fields are missing
            $this->trace->output($this->errorlogtag."Creating missing user profile fields.");

            $cat = $DB->get_record('user_info_category', array('name'=>'WISC fields'));
            if (empty($cat)) {
                $cat = new stdClass;
                $cat->name = 'WISC fields';
                $cat->id = $DB->insert_record('user_info_category', $cat);
            }
            $field = new stdClass;
            $field->visible = 0;
            $field->locked = 1;
            $field->required = 0;
            $field->categoryid = $cat->id;

            if (false === array_search('netid', $fields)) {
                $field->shortname = 'netid';
                $field->name = 'NetID';
                $field->datatype = 'text';
                $field->defaultdata = '';
                $field->description = 'Account has a NetID without scope. This field is provided by the WISC services plugin';
                $DB->insert_record('user_info_field', $field);
            }
            if (false === array_search('ferpaName', $fields)) {
                $field->shortname = 'ferpaName';
                $field->name = 'FERPA name';
                $field->datatype = 'checkbox';
                $field->defaultdata = 0;
                $field->description = 'Account has a FERPA protected name. This field is provided by the WISC services plugin';
                $DB->insert_record('user_info_field', $field);
            }
            if (false === array_search('ferpaEmail', $fields)) {
                $field->shortname = 'ferpaEmail';
                $field->name = 'FERPA email';
                $field->datatype = 'checkbox';
                $field->defaultdata = 0;
                $field->description = 'Account has a FERPA protected email address. This field is provided by the WISC services plugin';
                $DB->insert_record('user_info_field', $field);
            }
            if (false === array_search('emplid', $fields)) {
                $field->shortname = 'emplid';
                $field->name = 'UW EMPLID';
                $field->datatype = 'text';
                $field->defaultdata = '';
                $field->description = 'UW EMPLID (ISIS ID). This field is provided by the WISC services plugin';
                $DB->insert_record('user_info_field', $field);
            }
            if (false === array_search('photoid', $fields)) {
                $field->shortname = 'photoid';
                $field->name = 'UW PHOTOID';
                $field->datatype = 'text';
                $field->defaultdata = '';
                $field->description = 'UW PHOTOID. This field is provided by the WISC services plugin';
                $DB->insert_record('user_info_field', $field);
            }
            if (false === array_search('uwRoles', $fields)) {
                $field->shortname = 'uwRoles';
                $field->name = 'UW Roles';
                $field->datatype = 'text';
                $field->defaultdata = '';
                $field->description = 'ISIS Roles. This field is provided by the WISC services plugin';
                $DB->insert_record('user_info_field', $field);
            }
            if (false === array_search('uwOverride', $fields)) {
                $field->shortname = 'uwOverride';
                $field->name = 'UW information override';
                $field->datatype = 'menu';
                $field->defaultdata = self::OVERRIDE_NONE;
                $field->param1 = self::OVERRIDE_NONE."\n".self::OVERRIDE_NAME."\n".self::OVERRIDE_EMAIL."\n".self::OVERRIDE_ALL;
                $field->description = 'Which profile fields should preserve local changes. This field is provided by the WISC services plugin';
                $DB->insert_record('user_info_field', $field);
            }
            if (false === array_search('uwNoEmail', $fields)) {
                $field->shortname = 'uwNoEmail';
                $field->name = 'No upstream email';
                $field->datatype = 'checkbox';
                $field->defaultdata = 0;
                $field->description = 'We have no upstream email address for this user, so they can change the moodle email themselves. This is automatically set or cleared depending on upstream data and probably shouldn\'t be changed manually. This field is provided by the WISC services plugin.';
                $DB->insert_record('user_info_field', $field);
            }
            // Reload the list of fields
            list($where, $params) = $DB->get_in_or_equal($fieldshortnames, SQL_PARAMS_QM);
            $fields = $DB->get_records_select_menu('user_info_field', "shortname $where", $params, '', 'id,shortname');
        }
        $this->custom_fields = array_flip($fields);
        return $this->custom_fields;
    }

    /**
     * See if an email is in the list of system-wide bogus emails.
     *
     * @param string $email
     * @return boolean
     */
    public function is_valid_email($email) {
        static $bogusemails; // static hash of bogus emails from file.

        if (empty($email)) {
            return false;
        }

        if (!is_array($bogusemails)) {
            $bogusemails = array();
            // Initialize list of bogus emails from a file.
            $bogusemailfile = get_config('local_wiscservices', 'bogusemailfile');
            if (!empty($bogusemailfile)) {
                $lines = file($bogusemailfile, FILE_IGNORE_NEW_LINES|FILE_SKIP_EMPTY_LINES);
                if ($lines) {
                    foreach ($lines as $line) {
                        $line = trim(preg_replace('/#.*/','', $line));
                        if (!empty($line)) {
                            $bogusemails[strtolower(trim($line))] = true;
                        }
                    }
                }
            }
        }

        return !isset($bogusemails[strtolower($email)]);
    }

    /**
     * Shibboleth login hook.
     *
     * This is called from auth/shibboleth/index.php to process an account before shibboleth logs the user in.
     *
     * This function does nothing if a moodle account is present for the current shib login.
     * (i.e. if the shibboleth username matches an existing moodle account)
     *
     * If no account is present, then the function does the following:
     *
     * - Search for existing accounts by pvi, emplid and netid before creating a new accounts.
     *     This can eliminate duplicate accounts in many instances when a netid changes.
     * - Re-enable authtype="nologin" accounts as necessary at login time
     * - Create new accounts using People picker (getIMSPeople) data.  The means we do
     *     not rely on shibboleth attributes beyound pvi and netid.
     * - Block account creation when there is an inconsistency in the doit data sources.
     * - Block Guest NetID account creation, when so configured.
     *
     * @param string $shibusername eppn
     * @param string $pvi
     * @param datasource $uds for testing.  Defaults to standard UDS datasource.
     */
    static function shibboleth_login_hook($shibusername, $pvi, datasource $uds = null) {
        global $DB,$CFG;
        if (get_config('local_wiscservices', 'authtype') != 'shibboleth') {
            // Not configured to use shibboleth accounts
            return;
        }
        $moodleuser = $DB->get_record('user', array('username'=>$shibusername, 'deleted'=>0, 'suspended'=>0, 'auth'=>'shibboleth', 'mnethostid'=>$CFG->mnet_localhost_id), 'id, auth');
        if (!$moodleuser) {
            // User doesn't exist or is disabled
            $wiscservices = new local_wiscservices_plugin();
            if ($uds) {
                $wiscservices->set_datasource($uds);
            }
            $netid = $wiscservices->username_to_netid($shibusername);
            if (empty($netid)) {
                // This isn't a netid account (for example if the eppn doesn't match @wisc.edu), so just return.
                return;
            }

            // Check for Guest NetIDs
            $isguestnetid = strpos($netid, 'tmp-') === 0;
            if ($isguestnetid) {
                if ($isguestnetid && !get_config('local_wiscservices', 'allowguestnetids')) {
                    throw new moodle_exception('loginguestnetiderror', 'local_wiscservices');
                } else {
                    // People picker won't have the account, so we'll just let auth/shibbleth
                    // create the account
                    return;
                }
            }
            // Check for PVI
            if (empty($pvi)) {
                // Don't allow new accounts with no pvi
                throw new moodle_exception('loginmissingpvi', 'local_wiscservices');
            }

            // Look up person in People picker by PVI
            $datasource = $wiscservices->get_datasource();
            if (!$datasource) {
                throw new moodle_exception('logindatasourceerror', 'local_wiscservices');
            }
            $person = uds_query::get_people_by_pvi($datasource, $pvi);
            $person = reset($person);
            if (!$person) {
                throw new moodle_exception('logindatasourcenotfound', 'local_wiscservices');
            }
            if (empty($person->netid)) {
                // Apparently Shibboleth can get netid's before our datasource, and this will result
                // in verify_person creating a duplicate account.  We fix that here as we already
                // know the user's netid.
                $person->netid = $wiscservices->username_to_netid($shibusername);
                error_log('People picker returned no netid for user '.$shibusername);
            } else if ($person->netid != $wiscservices->username_to_netid($shibusername)) {
                // People picker gave us a different netid back than shibboleth had.  We don't want to create the new account
                // since then shibboleth will just make a new one on its own anyhow and we'll end up with
                // two accounts.
                throw new moodle_exception('logindatasourcewrongnetid', 'local_wiscservices');
            }
            if (!$person->valid_for_create()) {
                // We require a last name.
                throw new moodle_exception('logindatasourcenotcomplete', 'local_wiscservices');
            }
            // At this point $person->netid is guaranteed to match the login user name, so auth/shibboleth won't create a duplicate
            // account after we make one here.

            // All went well, so let verfiy_person() make/update the account
            $wiscservices->verify_person($person, true);
        }
    }

}
