<?php
$string['pluginname'] = 'WISC services plugin';
$string['pluginname_desc'] = '<p>This plugin syncs account data for University of Wisconsin - Madison users.</p>';

$string['phpsoap_noextension'] = '<h4>The PHP SOAP extension must be enabled to use this plugin</h4>';

$string['peoplepickerserver_settings'] = 'UW People Picker settings';
$string['peoplepickerurl_key'] = 'People Picker Server URL';
$string['peoplepickeruser_key'] = 'People Picker Username';
$string['peoplepickerpass_key'] = 'People Picker Password';

$string['udspersonserver_settings'] = 'UW GetUDSPerson settings';
$string['udspersonurl_key'] = 'GetUDSPerson Server URL';
$string['udspersonuser_key'] = 'GetUDSPerson Username';
$string['udspersonpass_key'] = 'GetUDSPerson Password';

$string['udsservice_settings'] = 'UDS Service settings (for getPVIChangeHistory)';
$string['udsserviceurl_key']  = 'UDS Service URL';
$string['udsserviceuser_key'] = 'UDS Username';
$string['udsservicepass_key'] = 'UDS Password';

$string['fakemixedcase_key'] = 'Guess mixed case name';
$string['fakemixedcase'] = 'CHUB already has mixed case names for all instructors and students, so this only applies to users we don\'t get through CHUB';

$string['account_settings'] = 'UW account settings';

$string['authtype_key'] = 'Auhentication plugin';
$string['authtype'] = 'Which authentication plugin to use for new accounts created from roster data.';

$string['allowguestnetids_key'] = 'Allow Guest NetIDs';
$string['allowguestnetids'] = 'If enabled, Guest NetIDs will be allowed to login and create new accounts.  This has no effect on existing accounts.';

$string['unsuspenduser'] = "Unsuspending userid {\$a}";
$string['updateuser'] = "Updating userid {\$a}";
$string['createduser'] = "Creating userid {\$a}";

$string['unenrolaction_key'] = 'External unenrol action';
$string['unenrolaction'] = 'Select action to carry out when a student drops a course.  This setting only applies to students that drop the course after the course begins.  Students that drop before the course begins are always unenrolled' ;

$string['development_key'] = 'Development server';
$string['development'] = 'Enabling this will prevent email addresses from being added to automatically generated UW accounts.' ;

$string['bogusemailfile_key'] = 'Bogus emails file';
$string['bogusemailfile'] = 'File which contains invalid upstream email addresses which we shouldn\'t use in moodle.  File format is one email address per line.  Use # for comments.' ;
$string['bogusemail'] = '{$a} (in bogus list)';

$string['wiscservices:adduser'] = 'Add a new user to the system by NetID';
$string['wiscservices:enroldialogadduser'] = 'Can search and add a new user to the system by NetID inside an enrollment dialog';

$string['adduwpeople'] = "Add UW people";
$string['datasource'] = "UW Identifier";
$string['uwids'] = "Accounts";
$string['uwid'] = "Search";
$string['netid'] = "Netid";
$string['pvi'] = "PVI";
$string['uwids_help'] = "Accepts a comma-separated or space-separated list of identifiers";
$string['addpeople'] = "Update people";
$string['bulkuser'] = "Bulk user actions";
$string['cantaddbyname'] = "Previewing users only.  To update users in bulk, please search by a personal identifier.";
$string['erroruser'] = "An error occured while adding account for <b>'{\$a}'</b>.";
$string['processeduser'] = "Processed {\$a}";
$string['netid'] = "NetID";
$string['uwroles'] = "UW roles";
$string['usercreate'] = "Create";
$string['userupdate'] = "Update";
$string['usernotfound'] = "Error looking up {\$a}";

$string['loginguestnetiderror'] = 'This service is not available to Guest NetIDs';
$string['loginmissingpvi'] = 'Some attributes on your account were not passed from the login server.  Please try again later.';
$string['logindatasourecerror'] = 'There was an error in contacting the university dataservice.  Please try again later.';
$string['logindatasourcenotcomplete'] = 'Your UW account seems to be missing some required data.  Please try again later.';
$string['logindatasourcenotfound'] = 'Your account was not found.  Please try again later.';
$string['logindatasourcewrongnetid'] = 'An inconsistency was found in your account information.  Please try again later.';

$string['caeapi_settings'] = 'CAE API Settings';
$string['caeusername'] = 'CAE Username';
$string['caepassword'] = 'CAE Password';
$string['cachedef_caeids'] = 'This caches College of Engineering ids.';

$string['nosamltoken'] = 'Unable to get saml token';
$string['getaccounterror'] = 'Unable to get the CAE Account';
$string['nocaeid'] = 'Unable to get CAE ID';


