<?php
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * UDSPerson class
*
* @package    local
* @subpackage wiscservices
* @copyright  2015 University of Wisconsin
* @author     Matt petro
* @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
*/

namespace local_wiscservices\local\wisc;

defined('MOODLE_INTERNAL') || die();

/**
 * wisc_person class.
 *
 * CHUB, Peoplepicker and UDSPerson people classes derive from this
 *
 * @package    local
 * @subpackage wiscservices
 * @copyright  2015 University of Wisconsin
 * @author     Matt petro
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class wisc_person {
    /** @var string */
    public $firstName;

    /** @var string */
    public $middleName;

    /** @var string */
    public $lastName;

    /** @var string */
    public $emplid;

    /** @var string */
    public $pvi; // one of netid or pvi is guaranteed to be nonempty

    /** @var string */
    public $netid;

    /** @var string */
    public $email;

    /** @var bool */
    public $ferpaName;

    /** @var bool */
    public $ferpaEmail;

    /**
     * Is this a valid record for use in moodle?  In particular, do we have enough data to match
     * to a moodle account.
     *
     * @return bool
     */
    public function valid() {
        return !empty($this->netid) || !empty($this->pvi);
    }

    /**
     * Is there sufficient data to create a new moodle account?
     *
     * @return bool
     */
    public function valid_for_create() {
        return !(empty($this->lastName) || empty($this->pvi) || empty($this->netid));
    }

    /**
     * Does this wisc_person object contain the preferred name if available?
     *
     * Derived classes can return false if there is no preferred name support (e.g. getIMSPeople)
     *
     * @return boolean
     */
    public function supports_preferred_names() {
        return true;
    }
}
