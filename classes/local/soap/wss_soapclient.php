<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * WS security wrapper for SoapClient class
 *
 * @package    local_wiscservices
 * @copyright  2015 University of Wisconsin - Madison
 * @author     Matt Petro
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace local_wiscservices\local\soap;


/**
 * WS security wrapper for SoapClient class
 *
 * This class adds a WS Security wrapper to all outgoing soap calls.  It
 * currently only supports plaintext password authentication.
 *
 * This class also provides a SOAP timeout mechanism, see bug
 * http://bugs.php.net/bug.php?id=48524
 *
 * Timeout code is based on: http://www.darqbyte.com/2009/10/21/timing-out-php-soap-calls/
 *
 * @package    local_wiscservices
 * @copyright  2015 University of Wisconsin - Madison
 * @author     Matt Petro
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 *
 */
class wss_soapclient extends \SoapClient
{
    private $username;
    private $password;
    private $samlxml;
    private $timeout;

    private $curl;
    private $location;

    static protected $performanceinfo = "";

    /**
     * Set the security token
     *
     * @param string $username
     * @param string $password
     */
    public function __setUsernameToken($username, $password) {
        $this->username = $username;
        $this->password = $password;
    }

    /**
     * Set the SAML security token
     *
     * @param string $samlxml]
     */
    public function __setSAMLToken($samlxml = null) {
        $this->samlxml = $samlxml;
    }

    /**
     * Set the timeout in seconds
     *
     * @param int $timeout
     */
    public function __setTimeout($timeout)
    {
        if (!is_int($timeout) && !is_null($timeout))
        {
            throw new \Exception("Invalid timeout value");
        }

        $this->timeout = $timeout;
    }


    /**
     * Add security headers to the request
     *
     * @param string $request
     */
    private function __wssecurity_header($request) {
        // modify request XML using the DOM API

        $noncedata = mt_rand();
        $timestamp = gmdate('Y-m-d\TH:i:s\Z');

        $domRequest = new \DOMDocument();
        $domRequest->loadXML($request);

        $xp = new \DOMXPath($domRequest);
        $xp->registerNamespace('soapenv', 'http://schemas.xmlsoap.org/soap/envelope/');

        $header = $xp->query('/soapenv:Envelope/soapenv:Header')->item(0);
        if ($header === null) {
            $header = $domRequest->createElementNS('http://schemas.xmlsoap.org/soap/envelope/', 'soapenv:Header');
            $envelope = $xp->query('/soapenv:Envelope')->item(0);
            $envelope->insertBefore($header, $envelope->firstChild);
        }

        // Add WSS security header
        $security = $domRequest->createElementNS('http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd', 'wsse:Security');
        $security->setAttributeNode(new \DOMAttr('soapenv:mustUnderstand', '0'));
        $security->setAttribute('xmlns:wsu', 'http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd');
        $usernameToken = $domRequest->createElement('wsse:UsernameToken');
        $username = $domRequest->createElement('wsse:Username', htmlspecialchars($this->username));
        $password = $domRequest->createElement('wsse:Password', htmlspecialchars($this->password));
        $password->setAttributeNode(new \DOMAttr('Type', 'http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordText'));
        $nonce = $domRequest->createElement('wsse:Nonce', base64_encode(pack('H*', $noncedata)));
        $nonce->setAttributeNode(new \DOMAttr('EncodingType', 'http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-soap-message-security-1.0#Base64Binary'));
        $created = $domRequest->createElement('wsu:Created', $timestamp);
        $security->appendChild($usernameToken);
        $usernameToken->appendChild($username);
        $usernameToken->appendChild($password);
        $usernameToken->appendChild($nonce);
        $usernameToken->appendChild($created);
        $header->appendChild($security);

        $request = $domRequest->saveXML();
        return $request;
    }

    /**
     * Adds SAML XML authentication to the WS-Security header
     *
     * @param string $request
     * @return string The updated request object
     */
    private function __wsssecurity_header_saml($request){

        $domRequest = new \DOMDocument();
        $domRequest->loadXML($request);

        $xp = new \DOMXPath($domRequest);
        $xp->registerNamespace('soapenv', 'http://schemas.xmlsoap.org/soap/envelope/');

        $header = $xp->query('/soapenv:Envelope/soapenv:Header')->item(0);
        if ($header === null) {
            $header = $domRequest->createElementNS('http://schemas.xmlsoap.org/soap/envelope/', 'soapenv:Header');
            $envelope = $xp->query('/soapenv:Envelope')->item(0);
            $envelope->insertBefore($header, $envelope->firstChild);
        }

        // Add WSS security header
        $security = $domRequest->createElementNS('http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd',
                                                 'wsse:Security');
        //$security->setAttributeNode(new DOMAttr('soapenv:mustUnderstand', '1'));
        $security->setAttribute('xmlns:wsu', 'http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd');

        $samlDOM = $domRequest->createDocumentFragment();
        $samlDOM->appendXML($this->samlxml);

        $security->appendChild($samlDOM);
        $header->appendChild($security);

        $request = $domRequest->saveXML();



        return $request;
    }

    static protected function __logperformance($time, $request) {
        $matches = array();
        if (preg_match("/([a-zA-Z]+)Request/", $request, $matches)) {
            $action = $matches[1];
        } else {
            $action = "Unknown";
        }
        $strtime = sprintf("%4.3f", $time);
        self::$performanceinfo .= "<div class=\"notify\">{$strtime}s {$action}()</div>";
    }

    static public function __printperformance() {
        echo self::$performanceinfo;
    }


    /**
     * Override standard __doRequest call to add security headers and provide timeout
     */
    public function __doRequest($request, $location, $action, $version, $one_way = null)
    {
        global $CFG;
        $start = microtime();

        if(!empty($this->username) && !empty($this->password)){
            $newrequest = $this->__wssecurity_header($request);
        }else if(!empty($this->samlxml) ){
            $newrequest = $this->__wsssecurity_header_saml($request);
        }else{
                $newrequest = $request;
        }

        if (!$this->timeout)
        {
            // Call via parent because we require no timeout
            $response = parent::__doRequest($newrequest, $location, $action, $version, $one_way);
        }
        else
        {
            // Call via Curl and use the timeout

            // For our use there should only be one location, but check anyways
            if ($this->location === $location && $this->curl) {
                // reuse curl context
                $curl = $this->curl;
            } else {
                // setup a new curl context
                if ($this->curl) {
                    curl_close($this->curl);
                }

                $curl = curl_init($location);

                curl_setopt($curl, CURLOPT_VERBOSE, FALSE);
                curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE);
                curl_setopt($curl, CURLOPT_POST, TRUE);
                curl_setopt($curl, CURLOPT_HEADER, FALSE);

                if (!empty($CFG->wiscnoverifypeer)) {
                    // FOR DEVELOPMENT -- ignore cert
                    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
                }

                $this->location = (string)$location; // N.B. $location is passed by reference
                $this->curl = $curl;
            }
            curl_setopt($curl, CURLOPT_HTTPHEADER, array("Content-Type: text/xml",
                                                         "Connection: keep-alive",
                                                         "Keep-Alive: 30",
                                                         "SOAPAction: \"$action\""));
            curl_setopt($curl, CURLOPT_TIMEOUT, $this->timeout);
            curl_setopt($curl, CURLOPT_POSTFIELDS, $newrequest);
            $response = curl_exec($curl);

            //$response = preg_replace('/CIAResponse/', 'GetCoursesResponse', $response);

            if (curl_errno($curl))
            {
                throw new \Exception(curl_error($curl));
            }
        }

        self::__logperformance(microtime_diff($start, microtime()), $request);
        return $response;
    }

}