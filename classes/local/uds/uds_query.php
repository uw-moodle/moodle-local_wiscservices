<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Datasource query functions for UDS data
 *
 * @package    local_wiscservices
 * @copyright  2015 University of Wisconsin - Madison
 * @author     Matt Petro
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace local_wiscservices\local\uds;

defined('MOODLE_INTERNAL') || die;

/**
 * Datasource query functions for UDS data
 *
 * @package    local_wiscservices
 * @copyright  2015 University of Wisconsin - Madison
 * @author     Matt Petro
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
abstract class uds_query {

    /**
     * Query users by pvi
     *
     * @param local_wiscservices\local\uds\datasource datasource
     * @param string|array $pvis
     * @return local_wiscservices\local\uds\person[]
     */
    public static function get_people_by_pvi(datasource $datasource, $pvis) {
        $queries = $datasource->build_people_query('UWMSNSUDS', 'pvi', $pvis);
        return $datasource->get_people($queries);
    }

    /**
     * Query users by phptoid
     *
     * @param local_wiscservices\local\uds\datasource datasource
     * @param string|array $photoids
     * @return local_wiscservices\local\uds\person[]
     */
    public static function get_people_by_photoid(datasource $datasource, $photoids) {
        $queries = $datasource->build_people_query('UWMSNSUDS', 'photoid', $photoids);
        return $datasource->get_people($queries);
    }

    /**
     * Query users by emplid
     *
     * @param local_wiscservices\local\uds\datasource datasource
     * @param string|array $emplid
     * @return local_wiscservices\local\uds\person[]
     */
    public static function get_people_by_emplid(datasource $datasource, $emplids) {
        $queries = $datasource->build_people_query('UWHRS', 'emplid', $emplids);
        return $datasource->get_people($queries);
    }

    /**
     * Query users by netid
     *
     * @param local_wiscservices\local\uds\datasource datasource
     * @param string|array $netids
     * @return local_wiscservices\local\uds\person[]
     */
    public static function get_people_by_netid(datasource $datasource, $netids) {
        $queries = $datasource->build_people_query('UWMSNSUDS', 'netid', $netids);
        return $datasource->get_people($queries);
    }

    /**
     * Query users by name
     *
     * @param local_wiscservices\local\uds\datasource datasource
     * @param string|array $names
     * @return local_wiscservices\local\uds\person[]
     */
    public static function get_people_by_name(datasource $datasource, $names) {
        $queries = $datasource->build_people_query('Name', '', $names);
        return $datasource->get_people($queries);
    }

}