<?php
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * UDSPerson client
 *
 * @package    local
 * @subpackage wiscservices
 * @copyright  2015 University of Wisconsin
 * @author     Matt petro
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace local_wiscservices\local\uds;

defined('MOODLE_INTERNAL') || die();

/**
 * wisc_udsperson class
 *
 * UDSPerson client
 *
 * @package    local
 * @subpackage wiscservices
 * @copyright  2015 University of Wisconsin
 * @author     Matt petro
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class uds_datasource implements datasource {

    const MAX_QUERY_SIZE = 25; // udsperson will handle up to 25 queries at once.  It silently ignores larger queries.

    /** @var uds_person_soapclient */
    protected $udspersonclient;

    /** @var uds_service_soapclient */
    protected $udsserviceclient;

    /** @var int */
    protected $timeout = 20;

    /** @var bool */
    protected $devmode;

    public function __construct() {
        $this->devmode = (boolean) get_config('local_wiscservices', 'development');
    }

    /**
     * Get udsclient, creating it if necessary.
     */
    protected function get_uds_person_client() {
        if (is_null($this->udspersonclient)) {
            $this->udspersonclient = uds_person_soapclient::get();
        }
        return $this->udspersonclient;
    }

    /**
     * Get udsclient, creating it if necessary.
     *
     *  Will return false if the service is not configured.
     */
    protected function get_uds_service_client() {
        if (is_null($this->udsserviceclient)) {
            $this->udsserviceclient = uds_service_soapclient::get(false);
        }
        return $this->udsserviceclient;
    }

    /**
     * Set developer mode (i.e. email addresses are obscured)
     *
     * @param boolean $dev
     */
    public function set_devmode($dev) {
        $this->devmode = $dev;
    }

    /**
     * Get developer mode (i.e. email addresses are obscured)
     *
     * @return boolean
     */
    public function get_devmode() {
        return $this->devmode;
    }

    /**
     * Set timeout
     *
     * @param int $timeout timeout in seconds
     */
    public function set_timeout($timeout) {
        $this->timeout = $timeout;
    }

    /**
     * Get timeout
     *
     * @return int
     */
    public function get_timeout() {
        return $this->timeout;
    }

    /**
     * Maximum number of users that can be queried at once.  get_people will handle larger queries,
     * but it will involve multiple SOAP calls.
     *
     * @return int
     */
    public function get_maxquery() {
        return static::MAX_QUERY_SIZE;
    }

    /**
     * Call getUDSPerson on an array of Person queries
     *
     * @param array $queries for example, as returned from build_people_query
     * @throws coding_exception
     * @throws SoapFault
     * @throws moodle_exception
     * @return array of wisc_uds_person
     */
    public function get_people(array $queries) {
        if (!is_null($this->timeout)) {
            $this->get_uds_person_client()->__setTimeout($this->timeout);
        }

        if (count($queries) > static::MAX_QUERY_SIZE) {
            // query is too large for the soap service, so chunk the array
            $output = array();
            foreach (array_chunk($queries, static::MAX_QUERY_SIZE) as $chunk) {
                $output[] = $this->get_people($chunk);
            }
            return call_user_func_array('array_merge', $output);
        }

        if (empty($queries)) {
            return array();
        }

        $params = array('People'=>$queries);
        $resp = $this->get_uds_person_client()->getUDSPerson($params);

        $statuscode = $resp->Status->Statuscode;
        if ($statuscode != '200') {
            throw new \SoapFault("$statuscode", 'Error returned from getUDSPerson');
        }

        $wiscpeople = array();
        if (isset($resp->People->Person)) {
            foreach ($resp->People->Person as $person) {
                $wiscperson = $this->create_wisc_uds_person($person);
                if (!$wiscperson) {
                    throw new \moodle_exception("Unable to create wisc_person from soap response");
                }
                $wiscpeople[] = $wiscperson;
            }
        }
        return $wiscpeople;
    }

    /**
     * Convert a list of values to a list of identifiers for use in getUDSPerson
     *
     * @param string $source
     * @param string $idname
     * @param string|array $values
     *
     * @return array of identifiers
     */
    public function build_people_query($source, $idname, $values) {
        if (!is_array($values)) {
            $values = array($values);
        }
        $people = array();
        foreach ($values as $value) {
            $person = new \stdClass;
            $person->Identifiers[] = (object) array('Source'=>$source, 'IdName'=>$idname, 'Value'=>$value);
            $people[] = $person;
        }
        return $people;
    }

    /**
     * Create a wisc_uds_person object from the getUDSPerson People record
     *
     * @param object $person
     * @return NULL|uds_person NULL on error
     */
    protected function create_wisc_uds_person(\stdClass $person) {
        global $CFG;

        $o = new uds_person();

        // Name
        // Use in order of preference: Preferred, Student, Demographic  (UWMOODLE-748)

        if (isset($person->Demographic->Name->Preferred_First)) {
            $o->firstName = $person->Demographic->Name->Preferred_First;
        } else if (isset($person->Student->Name->First)) {
            $o->firstName = $person->Student->Name->First;
        } else if (isset($person->Demographic->Name->First)) {
            $o->firstName = $person->Demographic->Name->First;
        } else {
            // No first name, leave null
        }

        if (isset($person->Demographic->Name->Preferred_Middle)) {
            $o->middleName = $person->Demographic->Name->Preferred_Middle;
        } else if (isset($person->Student->Name->Middle)) {
            $o->middleName = $person->Student->Name->Middle;
        } else if (isset($person->Demographic->Name->Middle)) {
            $o->middleName = $person->Demographic->Name->Middle;
        } else {
            // No middle name
        }

        if (isset($person->Student->Name->Last)) {
            $o->lastName = $person->Student->Name->Last;
        } else if (isset($person->Demographic->Name->Last)) {
            $o->lastName = $person->Demographic->Name->Last;

        } else {
            // No last name
        }

        // In some cases name will be null, but this might be okay for existing accounts as
        // at least we'll be able to keep the identifiers updated.

        // Email
        // Prefer Student, then Demographic.  (Will these always agree?)
        if (isset($person->Student->Email)) {
            $o->email = $person->Student->Email;
        } else if (isset($person->Demographic->Email)) {
            $o->email = $person->Demographic->Email;
        } else {
            $o->email = null;
        }

        // Overwrite email if in devmode
        if ($this->get_devmode()) {
            $o->email = $CFG->noreplyaddress;
        }

        // Identifiers
        if (isset($person->Identifiers->Identifier)) {
            foreach ( $person->Identifiers->Identifier as $identifier ) {
                switch ( strtoupper($identifier->IdName) ) {
                    case "NETID":
                        $o->netid = $identifier->Value;
                        break;
                    case "EMPLID":
                        $o->emplid = $identifier->Value;
                        break;
                    case "PVI":
                        $o->pvi = $identifier->Value;
                        break;
                    case "PHOTOID":
                        $o->photoid = substr($identifier->Value, 0, 10); // Omit issue number.
                        break;
                }
            }
        }

        // ISIS roles
        $o->roles = array();
        if (isset($person->Roles->Role)) {
            foreach ($person->Roles->Role as $role) {
                $o->roles[] = $role;
            }
        }

        // Ferpa flags
        $o->ferpaName = false;
        $o->ferpaEmail = false;
        if (isset($person->Demographic->PrivacyPolicies->Policy)) {
            foreach ($person->Demographic->PrivacyPolicies->Policy as $policy) {
                if (isset($policy->Attributes->Attribute)) {
                    foreach ($policy->Attributes->Attribute as $attribute) {
                        if ($attribute == 'PERSONAL EMAIL') {
                            $o->ferpaEmail = true;
                        } else if ($attribute == 'GENERAL NAME') {
                            $o->ferpaName = true;
                        }
                    }
                }
            }
        }

        // Validation

        // Can't have pvi and netid both empty
        if (!$o->valid()) {
            // Not a useful person for us
            return null;
        }

        return $o;
    }

    /**
     * Call getPVIChangeHistory
     *
     * @param string $pvi
     * @throws SoapFault
     * @return string[] array of pvis, first entry is current PVI.  Empty array if not found.
     */
    public function get_pvi_change_history($pvi) {

        $client = $this->get_uds_service_client();
        if (!$client) {
            // Return a dummy result if the UDS service is not configured.
            return array($pvi);
        }

        if (!is_null($this->timeout)) {
            $client->__setTimeout($this->timeout);
        }

        $params = array('pvi'=>$pvi);
        $resp = $client->getPVIChangeHistory($params);

        $statuscode = $resp->result;
        if ($statuscode == '204') { // No content
            return array();
        } else if ($statuscode != '200') {
            throw new \SoapFault("$statuscode", 'Error returned from getUDSPerson');
        }

        $result = array();
        if (isset($resp->PVIData->pvi)) {
            foreach ($resp->PVIData->pvi as $pvi) {
                $result[] = (string) $pvi;
            }
        }
        return $result;
    }
}
