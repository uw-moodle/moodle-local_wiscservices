<?php
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * UDSPerson soap client
 *
 * @package    local
 * @subpackage wiscservices
 * @copyright  2015 University of Wisconsin
 * @author     Matt petro
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace local_wiscservices\local\uds;
use local_wiscservices\local\soap\wss_soapclient;

defined('MOODLE_INTERNAL') || die();

/**
 * getudsperson_soapclient class
 *
 * Singleton class for communication with the UDSPerson service
 *
 * @package    local
 * @subpackage wiscservices
 * @copyright  2014 University of Wisconsin
 * @author     Matt petro
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class uds_person_soapclient extends wss_soapclient {

    protected function __construct($wsdl, $options = null) {
        parent::__construct($wsdl, $options);
    }


    /**
     * Returns the singleton instance of this class
     *
     * @exception SoapFault|Exception on error
     * @return wisc_soapclient|null
     */
    public static function get() {
        static $instance = null; // global instance of wisc_soapclient

        if ($instance === null) {

            $config = get_config("local_wiscservices");
            if (empty($config->udspersonurl)
                   || empty($config->udspersonuser)
                   || empty($config->udspersonpass)) {
                throw new \Exception("UDSPerson not configured");
            }

            $params = array( 'exceptions' => true,
                             'trace' => true,
                             'connection_timeout' => 20,
                             'features' => SOAP_SINGLE_ELEMENT_ARRAYS,
                           );

            $instance = new wss_soapclient($config->udspersonurl, $params);
            $instance->__setUsernameToken($config->udspersonuser, $config->udspersonpass);

            $instance->__setTimeout(20);
        }
        return $instance;
    }
}