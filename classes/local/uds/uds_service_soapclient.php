<?php
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * UDS Service soap client
 *
 * @package    local
 * @subpackage wiscservices
 * @copyright  2015 University of Wisconsin
 * @author     Matt petro
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace local_wiscservices\local\uds;
use local_wiscservices\local\soap\wss_soapclient;

defined('MOODLE_INTERNAL') || die();

/**
 * getudsperson_soapclient class
 *
 * Singleton class for communication with the UDSPerson service
 *
 * @package    local
 * @subpackage wiscservices
 * @copyright  2014 University of Wisconsin
 * @author     Matt petro
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class uds_service_soapclient extends wss_soapclient {

    protected function __construct($wsdl, $options = null) {
        parent::__construct($wsdl, $options);
    }


    /**
     * Returns the singleton instance of this class
     *
     * @param bool $throwexception If the service is unconfigured then throw an exception (true), or return null (false).
     * @exception SoapFault|Exception on error
     * @return wisc_soapclient|false
     */
    public static function get($throwexception = true) {
        static $instance = null; // global instance of wisc_soapclient

        if ($instance === null) {

            $config = get_config("local_wiscservices");
            if (empty($config->udsserviceurl)
                   || empty($config->udsserviceuser)
                   || empty($config->udsservicepass)) {

                if ($throwexception) {
                    throw new \Exception("UDS Service not configured");
                } else {
                    return false;
                }
            }

            $params = array( 'exceptions' => true,
                             'trace' => true,
                             'connection_timeout' => 20,
                             'features' => SOAP_SINGLE_ELEMENT_ARRAYS,
                           );

            $instance = new wss_soapclient($config->udsserviceurl, $params);
            $instance->__setUsernameToken($config->udsserviceuser, $config->udsservicepass);

            $instance->__setTimeout(20);
        }
        return $instance;
    }
}