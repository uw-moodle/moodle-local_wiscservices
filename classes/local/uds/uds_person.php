<?php
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * UDSPerson class
*
* @package    local
* @subpackage wiscservices
* @copyright  2015 University of Wisconsin
* @author     Matt petro
* @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
*/

namespace local_wiscservices\local\uds;

defined('MOODLE_INTERNAL') || die();

/**
 * uds_person class
 *
 * @package    local
 * @subpackage wiscservices
 * @copyright  2015 University of Wisconsin
 * @author     Matt petro
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
*/
class uds_person extends \local_wiscservices\local\wisc\wisc_person {
    // Same people info as CHUB, plus the infoaccess role and photoid

    /** @var string[] uds roles*/
    public $roles;

    // Faculty center uses only the first 10 digits of the photo id.
    /** @var string first 10 digits of photo id */
    public $photoid;

    public function supports_preferred_names() { return true; }
}