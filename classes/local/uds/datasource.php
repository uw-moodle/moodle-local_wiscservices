<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Datastore interface for UDS data
 *
 * @package    local_wiscservices
 * @copyright  2015 University of Wisconsin - Madison
 * @author     Matt Petro
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace local_wiscservices\local\uds;

defined('MOODLE_INTERNAL') || die;

/**
 * Datastore interface for UDS data
 *
 * @package    local_wiscservices
 * @copyright  2015 University of Wisconsin - Madison
 * @author     Matt Petro
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
interface datasource extends \local_wiscservices\local\soap\datasource {

    /**
     * Set developer mode (i.e. email addresses are obscured)
     *
     * @param bool $dev
     */
    public function set_devmode($dev);

    /**
     * Get developer mode (i.e. email addresses are obscured)
     *
     * @return bool
     */
    public function get_devmode();

    /**
     * Maximum number of users that can be queried at once.  get_people will handle larger queries,
     * but it will involve multiple SOAP calls.
     *
     * @return int
     */
    public function get_maxquery();

    /**
     * Call getUDSPerson on an array of Person queries
     *
     * @param array $queries for example, as returned from build_people_query
     * @throws coding_exception
     * @throws SoapFault
     * @throws moodle_exception
     * @return local_wiscservices\local\uds_person[]
     */
    public function get_people(array $queries);

    /**
     * Convert a list of values to a list of identifiers for use in getUDSPerson
     *
     * @param string $source
     * @param string $idname
     * @param string|array $values
     *
     * @return array of identifiers
     */
    public function build_people_query($source, $idname, $values);

    /**
     * Call getPVIChangeHistory
     *
     * @param string $pvi
     *
     * @return string[] array of pvis, first entry is current PVI.  Empty array if not found.
     */
    public function get_pvi_change_history($pvi);

}