<?php
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

namespace local_wiscservices\local\cae;

use local_wiscservices\local\soap\wss_soapclient;

defined('MOODLE_INTERNAL') || die();
global $CFG;

/**
 * caeauth_client class
 *
 * @package    local_wiscservices
 * @copyright  2014 University of Wisconsin
 * @author     John Hoopes <john.hoopes@wisc.edu>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class caeauth_client extends wss_soapclient {

    /** @const string CAEAUTH_WSDL The auth wsdl to get the SAML token to make requests on the account management page */
    const CAEAUTH_WSDL = 'https://caeapi.cae.wisc.edu/wsdls/caeauth/caeauth.wsdl';


    protected function __construct($wsdl, $options = null) {
        parent::__construct($wsdl, $options);
    }

    /**
     * Returns the singleton instance of this class
     *
     * @throws \Exception SoapFault|Exception on error
     * @return caeauth_client|null
     */
    public static function get() {
        static $instance = null; // global instance of wisc_soapclient

        if ($instance === null) {

            $params = array( 'exceptions' => true,
                'trace' => true,
                'connection_timeout' => 20,
                'features' => SOAP_SINGLE_ELEMENT_ARRAYS,
            );

            $instance = new self(self::CAEAUTH_WSDL, $params);

            $instance->__setTimeout(20);
        }
        return $instance;
    }
}