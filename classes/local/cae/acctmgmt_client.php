<?php
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

namespace local_wiscservices\local\cae;
use local_wiscservices\local\soap\wss_soapclient;

global $CFG;

/**
 * Soap Client class
 *
 * @package local_wiscservices
 * @author John Hoopes <hoopes@wisc.edu>
 * @copyright 2014 University of Wisconsin - Madison
 * @license http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class acctmgmt_client extends wss_soapclient
{
    /** @const string ACCTMGMT_WSDL The account managmenet wsdl to get account information and more from the CAE API  */
    const ACCTMGMT_WSDL = 'http://caeapi.cae.wisc.edu/wsdls/actmgmt/actmgmt_v1_0.wsdl';

    /**
     * Constructor class to call the parent constructor
     *
     * @param string $wsdl
     * @param array $options
     */
    public function __construct($wsdl, $options = null){
        parent::__construct($wsdl, $options);
    }

    /**
     * Returns the singleton instance of this class
     *
     * @param string|null $samltoken The string for the sam xml to add to the WSS header
     * @return acctmgmt_client|null
     * @throws \Exception Throws exception when there is no saml token
     */
    public static function get($samltoken = null){

        static $instance = null; // global instance of acctmgmt_client

        if ($instance === null) {

            if (empty($samltoken)){
                throw new \Exception("No SAML auth token");
            }

            $params = array( 'exceptions' => true,
                'trace' => true,
                'connection_timeout' => 20,
                'features' => SOAP_SINGLE_ELEMENT_ARRAYS,
            );

            $instance = new self(self::ACCTMGMT_WSDL, $params);
            $instance->__setSAMLToken($samltoken);

            $instance->__setTimeout(20);
        }
        return $instance;


    }

}