<?php

namespace local_wiscservices\getcaeid;

/**
 * Class to handle getting and using the CAE IDs from netid from the CAE API
 *
 * @package local_wiscservices
 * @author John Hoopes <hoopes@wisc.edu>
 * @copyright 2014 University of Wisconsin - Madison
 * @license http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class caeid
{

    /** @var \local_wiscservices\getcaeid\caeauth_client $caeauthclient */
    protected $caeauthclient;

    /** @var \local_wiscservices\getcaeid\acctmgmt_client $acctmgmtclient */
    protected $acctmgmtclient;

    /**
     * Construct the class
     *
     */
    public function __construct(){

    }

    /**
     * Gets the CAE Login name
     *
     * @return bool|string
     */
    public function getcaelogin(){
        return $this->getcaelogin_fromapiorcache();
    }

    /**
     * Returns the string representation of the cae AD ID
     *
     * @return string|bool
     */
    public function get_caeid_activedirectory(){
        global $USER;
        try{
            $caelogin = $this->getcaelogin_fromapiorcache();
        }catch(\Exception $e){
            error_log('Exception thrown when trying to get CAE ID from NetID');
            echo "Unable to convert your login to CAE Login";
            return false;
        }


        if($caelogin){
            return $caelogin . '@ad.cae.wisc.edu';
        }else{
            return false;
        }

    }

    /**
     * Gets the cae login
     *
     * @return bool|string
     * @throws \moodle_exception
     */
    private function getcaelogin_fromapiorcache(){
        global $USER;

        $cache = \cache::make('local_wiscservices', 'caeids');

        if($data = $cache->get($USER->username)){

            if( (time() - $data['created']) < 604800 ){
                // if the caeid hasn't expired (7 days) return it, otherwise go through the getting process
                return $data['caeid'];
            }
        }

        $this->generate_clients();

        if (preg_match('/^(.*)@wisc\.edu$/', $USER->username, $matches)) {
            // wisc domain
            $netid = $matches[1];
        } else {
            // not wisc, so ignore this username
            return false;
        }

        $params = array(
            'GetCAEAccountBasicInfoByOtherIDRequest' => array(
                'otherid' => $netid,
            )
        );

        try{
            //$response = $this->acctmgmtclient->GetCAEAccountBasicInfoByOtherID($params);
            $response = $this->acctmgmtclient->__call("GetCAEAccountBasicInfoByOtherID", $params);
        }catch(\Exception $e){
            $x=0;
            throw new \moodle_exception('getaccounterror', 'local_wiscservices', null, null, $e->getMessage());
        }

        if(isset($response->account)){
            // return the active directory login
            $account = $response->account[0];

            $cacheval = array(
                'created' => time(),
                'caeid' => $account->caelogin,
            );
            $cache->set($USER->username, $cacheval);

            return $account->caelogin;
        }else{
            return false;
        }
    }

    /**
     * Generates the clients for the class
     *
     */
    private function generate_clients(){
        $this->caeauthclient = \local_wiscservices\getcaeid\caeauth_client::get();
        $samltoken = $this->get_samltoken();
        $this->acctmgmtclient = \local_wiscservices\getcaeid\acctmgmt_client::get($samltoken);
    }

    /**
     * gets the saml token to use with the acctmgmtclient
     *
     * @return string
     * @throws \Exception|\moodle_exception
     */
    private function get_samltoken(){
        global $CFG;

        $wiscservicesconfig = get_config('local_wiscservices');

        // first checked our cached version first
        if( (time() - $wiscservicesconfig->caeauthcreated ) <  18000){
            return $wiscservicesconfig->caeauth;
        }

        if(empty($wiscservicesconfig->caeusername) || empty($wiscservicesconfig->caepassword)){
            throw new \Exception("CAE Account not set up");
        }

        $params = array(
            'GetTokenFromUsernamePasswordRequest' => array(
                'loginname' => $wiscservicesconfig->caeusername,
                'password' => $wiscservicesconfig->caepassword,
                'application' => $CFG->wwwroot,
            )
        );

        try{
            //$response = $this->caeauthclient->GetSAMLTokenFromUsernamePassword($params);
            $response = $this->caeauthclient->__call("GetSAMLTokenFromUsernamePassword", $params);
        }catch(\Exception $e){
            $lastrequest = $this->caeauthclient->getLastRequest();
            $x = 0;

            throw new \moodle_exception('nosamltoken', 'local_wiscservices', null, null, $e->getMessage());
        }

        $samltoken = urldecode(base64_decode($response->SAMLToken, true));

        // Take out the XML header
        $samltoken = preg_replace('/<\?xml.*\?>/', '', $samltoken);

        // set the config vars for the saml token
        set_config('caeauthcreated', time(), 'local_wiscservices');
        set_config('caeauth', $samltoken, 'local_wiscservices');

        return $samltoken;
    }
}