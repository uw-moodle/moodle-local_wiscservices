<?php
/**
 * GetUDSPerson datastore compatibility class
 *
 * @package    local_wiscservices
 * @copyright  2015 University of Wisconsin - Madison
 * @author     Matt Petro
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

use \local_wiscservices\local\uds\uds_datasource;
use \local_wiscservices\local\uds\uds_query;

defined('MOODLE_INTERNAL') || die();

/**
 * GetUDSPerson datastore compatibility class
 *
 * @package    local_wiscservices
 * @copyright  2015 University of Wisconsin - Madison
 * @author     Matt Petro
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 *
 * @deprecated since 4.0 use \local_wiscservices\local\uds\uds_datasource class instead
 */
class wisc_getudsperson extends uds_datasource {

    /**
     * Query users by pvi
     *
     * @param string|array $pvis
     * @return wisc_uds_person[]
     *
     * @deprecated since 4.0 use \local_wiscservices\local\uds\uds_query class instead
     */
    public function get_people_by_pvi($pvis) {
        return uds_query::get_people_by_pvi($this, $pvis);
    }

    /**
     * Query users by phptoid
     *
     * @param string|array $photoids
     * @return wisc_uds_person[]
     *
     * @deprecated since 4.0 use \local_wiscservices\local\uds\uds_query class instead
     */
    public function get_people_by_photoid($photoids) {
        return uds_query::get_people_by_photoid($this, $photoids);
    }

    /**
     * Query users by emplid
     *
     * @param string|array $emplid
     * @return wisc_uds_person[]
     *
     * @deprecated since 4.0 use \local_wiscservices\local\uds\uds_query class instead
     */
    public function get_people_by_emplid($emplids) {
        return uds_query::get_people_by_emplid($this, $emplids);
    }

    /**
     * Query users by netid
     *
     * @param string|array $netids
     * @return wisc_uds_person[]
     *
     * @deprecated since 4.0 use \local_wiscservices\local\uds\uds_query class instead
     */
    public function get_people_by_netid($netids) {
        return uds_query::get_people_by_netid($this, $netids);
    }

    /**
     * Query users by name
     *
     * @param string|array $names
     * @return array of wisc_uds_person
     *
     * @deprecated since 4.0 use \local_wiscservices\local\uds\uds_query class instead
     */
    public function get_people_by_name($names) {
        uds_query::get_people_by_name($this, $names);
    }
}