<?php

require_once($CFG->dirroot.'/local/wiscservices/lib/peoplepicker_soapclient.php');

/**
 * This class defines an interface to the PeoplePicker service
 * @deprecated since 3.2  - Peoplepicker does not have preferred names.  Use UDS instead.
 */
class wisc_peoplepicker {

    const MAX_QUERY_SIZE = 50; // people picker will handle up to 50 queries at once

    protected $ppclient;
    protected $timeout;

    protected $dev;


    public function __construct() {
        $this->ppclient = peoplepicker_soapclient::get();
        $this->dev = get_config("local_wiscservices", "development");
    }

    public function setTimeout($timeout) {
       $this->timeout = $timeout;
    }

    protected function initTimeout() {
        if (!is_null($this->timeout)) {
            $this->ppclient->__setTimeout($this->timeout);
        }
    }

    public function getPeopleByPvi($pvis) {
        return $this->getPeople($pvis, "UW-Madison UDS");
    }

    public function getPeopleByEmplid($emplids) {
        return $this->getPeople($emplids, "UW-Madison ISIS");
    }

    public function getPeopleByNetid($netids) {
        return $this->getPeople($netids, "UW-Madison NetID");
    }

    public function getPeopleByHrid($hrids) {
        return $this->getPeople($hrids, "UW-Madison HR");
    }

    public function getMaxQuery() {
        return wisc_peoplepicker::MAX_QUERY_SIZE;
    }

    public function getPeople($ids, $source) {
        $this->initTimeout();
        if (!is_array($ids)) {
            $ids = array($ids);
        }
        if (count($ids) > wisc_peoplepicker::MAX_QUERY_SIZE) {
            throw new Exception("Peoplepicker: query too large");
        }

        $locateusers = array ();
        foreach ($ids as $id) {
            $locateusers[] = array("source" => $source, "id" => $id);
        }
        $params = array('People' => array ('sourcedid' => $locateusers));
        $resp = $this->ppclient->getIMSPeople($params);

        if (empty($resp->enterprise->person)) {
            return array();
        }
        $people = $resp->enterprise->person;
        $resp = array();
        foreach ( $people as $person ) {
             $p = $this->createPerson($person);
             if ($p) {
                 $resp[] = $p;
             }
        }
        return $resp;
    }

    protected function createPerson($person) {
        global $CFG;

        $o = new wisc_peoplepicker_person();
        $o->firstName = isset($person->name->n->given)? $person->name->n->given : '';
        $o->lastName  = $person->name->n->family;
        $o->middleName = '';
        if (isset($person->name->n->partname)) {
            foreach ($person->name->n->partname as $part) {
                if ($part->partnametype == "middle") {
                    $o->middleName = $part->_;
                    break;
                }
            }
        }

        if (!$this->dev) {
            $o->email = isset($person->email)? $person->email : '';
        } else {
            $o->email = $CFG->noreplyaddress;
        }

        foreach ( $person->userid as $userid ) {
            switch ( (string)$userid->useridtype ) {
                case "UW-Madison NetID":
                    $o->netid = $userid->_;
                    break;
                case "UW-Madison HR":
                    $o->hrid = $userid->_;
                    break;
                case "UW-Madison ISIS":
                    $o->emplid = $userid->_;
                    break;
                case "UW-Madison UDS":
                    $o->pvi = $userid->_;
                    break;
            }
        }
        if (is_array($person->extension->any)) {
            // php version >= 5.3
            $uds = $person->extension->any['UDS'];
        }
        else {
            // php version <= 5.2
            // Translate the extension field into an object
            // because it is defined as an "any" field, the php parser just returns a string for it.
            $newtext = str_replace ( "udsroles:", "", $person->extension->any );
            $extension = (array) simplexml_load_string( '<t>'.$newtext.'</t>' ); // outermost tags get stripped
            $uds = $extension['UDS'];
        }
        $o->ferpaName = "Y" === ( string ) $uds->ferpaAttributes->name;
        $o->ferpaEmail = "Y" === ( string ) $uds->ferpaAttributes->email;

        if (!empty($uds->uwRole)) {
            $o->roles = $uds->uwRole;
        } else {
            $o->roles = array();
        }
        if (empty($o->netid) && empty($o->pvi)) {
            // not a useful record to us
            return null;
        }
        return $o;
    }
}

/**
 * @deprecated since 3.2
 * @author petro
 */
class wisc_peoplepicker_person extends local_wiscservices\local\wisc\wisc_person {
    // Same people info as CHUB, plus the infoaccess role

    public $roles;  // array of strings

    public function supports_preferred_names() { return false; } // Will never be added

    //  UDS roles:
    //         "Instructor"
    //         "EnrolledStudent"
    //         "WithdrawnStudent"
    //         "EligibleStudent"
    //         "Applicant"
    //         "Connections"
    //         "Faculty"
    //         "LimitedEmployee"
    //         "AcademicStaff"
    //         "EmployeeInTraining"
    //         "ClassifiedPermanent"
    //         "ClassifiedLTE"
    //         "ClassifiedProject"
    //         "StudentAssistant"
    //         "StudentHelp"
    //         "OtherEmployee"
    //         "HospitalEmployee"
    //         "FormerEmployee"
    //         "SpecAuth"

}
