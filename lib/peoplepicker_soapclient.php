<?php

use \local_wiscservices\local\soap\wss_soapclient;

/**
 * Wisc enrollment SOAP client
 *
 * Singleton class for communication with the People Picker server
 * @deprecated since 3.2
 */
class peoplepicker_soapclient extends wss_soapclient {

    protected function __construct($wsdl, $options = null) {
        parent::__construct($wsdl, $options);
    }

    /**
     * Returns the singleton instance of this class
     *
     * @exception SoapFault|Exception on error
     * @return wisc_soapclient|null
     */
    public static function get() {
        static $instance = null; // global instance of peoplepicker_soapclient

        if ($instance === null) {

            $config = get_config("local_wiscservices");
            if (empty($config->peoplepickerurl)
                   || empty($config->peoplepickeruser)
                   || empty($config->peoplepickerpass)) {
                throw new Exception("Peoplepicker not configured");
            }

            $params = array( 'exceptions' => true,
                             'trace' => true,
                             'connection_timeout' => 20,
                             'features' => SOAP_SINGLE_ELEMENT_ARRAYS,
                           );

            $instance = new peoplepicker_soapclient($config->peoplepickerurl, $params);
            $instance->__setUsernameToken($config->peoplepickeruser, $config->peoplepickerpass);

            $instance->__setTimeout(20);
        }
        return $instance;
    }
}