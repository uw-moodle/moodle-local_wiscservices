<?php
defined('MOODLE_INTERNAL') || die();

if ($show_instructions) {
    $columns = 'twocolumns';
} else {
    $columns = 'onecolumn';
}

if (!empty($CFG->loginpasswordautocomplete)) {
    $autocomplete = 'autocomplete="off"';
} else {
    $autocomplete = '';
}


if($bgrconfig = get_config('theme_big_red_responsive')){

    $netidlogintext = $bgrconfig->fplnetidbuttontext;
    $visitorlogintext = $bgrconfig->fpllocalloginbuttontext;
    $loginhelplink = $bgrconfig->fplhelplink;

}else{
    $netidlogintext = 'UW NetID Login';
    $visitorlogintext = 'Visitor Login';
    $loginhelplink = 'https://kb.wisc.edu/page.php?id=30448';
}


$netidbtn = '<p><a class="netidlink" rel="external" href="' . $CFG->httpswwwroot . '/auth/shibboleth/index.php">' . $netidlogintext . '</a></p>';
$visitorbtn = '<p><a class="visitorlink" href="' . $CFG->httpswwwroot . '/local/wiscservices/login/index.php?local=1">' . $visitorlogintext . '</a></p>';


// check for custom button layout
if(!empty($CFG->bgrloginbtnlayout)){

    $btnlayout = explode(',', $CFG->bgrloginbtnlayout);

    $buttons = '';
    foreach($btnlayout as $btnl){

        switch(trim($btnl)){
            case 'netid':
                $buttons .= $netidbtn;
                break;
            case 'visitorlink':
                $buttons .= $visitorbtn;
                break;
        }
    }

}else{
    $buttons = $netidbtn . $visitorbtn;
}


?>
<div id="wisclogin" class="loginbox clearfix <?php echo $columns ?>">
    <div class="loginpanel">
        <?php
        if (($CFG->registerauth == 'email') || !empty($CFG->registerauth)) { ?>
            <div class="skiplinks"><a class="skip" href="signup.php"><?php print_string("tocreatenewaccount"); ?></a></div>
        <?php
        } ?>
        <h2><?php print_string("login") ?></h2>
        <?php if (!$showloginform) { ?>
        <div class="subcontent loginsub">
            <div class="login">

                <p>Choose your login method:</p>
                <div class="buttons">
                    <?php echo $buttons; ?>
                </div>
                  <span class="smallLink">
                    <a target="blank" href="<?php echo $loginhelplink; ?>">Which login method should I use?</a>
                  </span>
            </div>
        </div>
        <?php if ($CFG->guestloginbutton and !isguestuser()) {  ?>
            <div class="subcontent loginsub">
                <div class="login">
                    <div class="desc">
                        <?php print_string("someallowguest") ?>
                    </div>
                    <form action="<?php echo $CFG->httpswwwroot; ?>/login/index.php" method="post" id="guestlogin">
                        <div class="guestform">
                            <input type="hidden" name="username" value="guest" />
                            <input type="hidden" name="password" value="guest" />
                            <input type="submit" value="<?php print_string("loginguest") ?>" />
                        </div>
                    </form>
                </div>
            </div>
        <?php } ?>
    </div>
    <?php } else { ?>
    <div class="subcontent loginsub">
        <div class="login">
            <div class="desc">
                Login below using your local moodle username <b>OR</b> email and password
            </div>
            <?php
            if (!empty($errormsg)) {
                echo '<div class="loginerrors">';
                echo $OUTPUT->error_text($errormsg);
                echo '</div>';
            }
            ?>
            <form action="<?php echo $CFG->httpswwwroot; ?>/login/index.php" method="post" id="login" <?php echo $autocomplete; ?> >
                <div class="loginform">
                    <div class="form-label"><label for="username"><?php print_string("username") ?></label></div>
                    <div class="form-input">
                        <input type="text" name="username" id="username" size="15" value="<?php p($frm->username) ?>" />
                    </div>
                    <div class="clearer"><!-- --></div>
                    <div class="form-label"><label for="password"><?php print_string("password") ?></label></div>
                    <div class="form-input">
                        <input type="password" name="password" id="password" size="15" value="" <?php echo $autocomplete; ?> />
                        <input type="submit" id="loginbtn" value="<?php print_string("login") ?>" />
                    </div>
                    <div class="clearer"><!-- --></div>
                    <?php if (isset($CFG->rememberusername) and $CFG->rememberusername == 2) { ?>
                        <div class="form-label"><input type="checkbox" name="rememberusername" id="rememberusername" value="1" <?php if ($frm->username) {echo 'checked="checked"';} ?> /></div>
                        <div class="form-input"><label for="rememberusername"><?php print_string('rememberusername', 'admin') ?></label></div>
                    <?php } ?>
                </div>
                <div class="clearer"><!-- --></div>
                <div class="forgetpass"><a href="<?php echo $CFG->httpswwwroot; ?>/login/forgot_password.php">Forgotten your visitor username or password?</a></div>
            </form>
        </div>
    </div>
    <div class="subcontent backsub">
        <form action="<?php echo $CFG->httpswwwroot; ?>/login/index.php" method="get">
            <div class="backform">
                <input type="submit" name="back" value="Back to login chooser" />
            </div>
        </form>
    </div>

</div>
<?php } ?>

<?php if ($show_instructions) { ?>
    <div class="signuppanel">
        <h2><?php print_string("firsttime") ?></h2>
        <div class="subcontent">
            <?php     if (is_enabled_auth('none')) { // instructions override the rest for security reasons
                print_string("loginstepsnone");
            } else if ($CFG->registerauth == 'email') {
                if (!empty($CFG->auth_instructions)) {
                    echo format_text($CFG->auth_instructions);
                } else {
                    print_string("loginsteps", "", $CFG->wwwroot.'/auth/instant/signup.php');
                } ?>
                <div class="signupform">
                    <form action="<?php echo "$CFG->wwwroot/auth/instant/signup.php"; ?>" method="get" id="signup">
                        <div><input type="submit" value="<?php print_string("startsignup") ?>" /></div>
                    </form>
                </div>
            <?php     } else if (!empty($CFG->registerauth)) {
                echo format_text($CFG->auth_instructions); ?>
                <div class="signupform">
                    <form action="<?php echo $CFG->wwwroot.'/auth/instant/signup.php'; ?>" method="post" id="signup">
                        <div><input type="submit" value="<?php print_string("startsignup") ?>" /></div>
                    </form>
                </div>
            <?php     } else {
                echo format_text($CFG->auth_instructions);
            } ?>
        </div>
    </div>
<?php } ?>
<?php if (!empty($potentialidps)) { ?>
    <div class="subcontent potentialidps">
        <h6><?php print_string('potentialidps', 'auth'); ?></h6>
        <div class="potentialidplist">
            <?php foreach ($potentialidps as $idp) {
                echo  '<div class="potentialidp"><a href="' . $idp['url']->out() . '" title="' . $idp['name'] . '">' . $OUTPUT->render($idp['icon'], $idp['name']) . '&nbsp;' . $idp['name'] . '</a></div>';
            } ?>
        </div>
    </div>
<?php } ?>
</div>
