<?php

defined('MOODLE_INTERNAL') || die();

require_once($CFG->dirroot.'/user/profile/lib.php');
require_once($CFG->dirroot.'/local/wiscservices/locallib.php');
require_once($CFG->dirroot.'/local/wiscservices/lib/peoplepicker.php');

/**
 * Event handler for WISC local plugin.
 */
class local_wiscservices_handler {
    static public function user_created($user) {
        global $CFG, $SESSION;
        $errortag = '[LOCAL WISC] user_created handler: ';

        if ($user->auth !== get_config('local_wiscservices', 'authtype')) {
            return true;
        }

        $myuser = clone $user; // clone so that we can make changes without affecting other handlers
        profile_load_custom_fields($myuser);

        $isaccountsetup = !empty($myuser->profile['uwRoles']) && !empty($myuser->lastname);
        // There's no reason to call sync_user if the name and uwRoles are already set.  Note that
        // for new accounts which came from CHUB, the name will be set, but not the uwRoles.
        if (!$isaccountsetup) {
            $wisc = new local_wiscservices_plugin();
            if (!$wisc->sync_user($myuser)) {
                error_log($errortag."PP returned nothing for user '{$user->username}'");
            }
        }


        return true;
    }
}